package com.zarbosoft.aggregateunused.generate;

import com.zarbosoft.automodel.task.AutoEnum;
import com.zarbosoft.automodel.task.AutoModel;
import com.zarbosoft.automodel.task.AutoObject;
import com.zarbosoft.automodel.task.AutoType;
import com.zarbosoft.automodel.task.ListType;
import org.decimal4j.immutable.Decimal4f;

public class GenerateModel extends TaskBase {
  @Override
  public void run() {
    new AutoModel("com.zarbosoft.docgraph.gui.model")
        .version(
            "v1",
            version -> {
              AutoEnum graphDirection =
                  version.enu("GraphDirection", "UP", "DOWN", "LEFT", "RIGHT");
              AutoEnum selectState = version.enu("SelectState", "NONE", "PRIMARY", "SECONDARY");
              AutoObject node =
                  version
                      .obj("Node")
                      .field("text", AutoType.string, f -> f.versioned().def("$S", ""))
                      .field("image", AutoType.string, f -> f.versioned().def("$S", ""))
                      .field("color", AutoType.string, f -> f.versioned().def("$S", ""))
                      .field("x", AutoType.ofDec(Decimal4f.class), f -> f.mutable())
                      .field("y", AutoType.ofDec(Decimal4f.class), f -> f.mutable())
                      .field("width", AutoType.ofDec(Decimal4f.class), f -> f.mutable())
                      .field("height", AutoType.ofDec(Decimal4f.class), f -> f.mutable())
                      .field("selected", selectState, f -> f.mutable())
                      .field("justCreated", AutoType.bool, f -> f.mutable().def("true"));
              node.field("children", new ListType(node), f -> f.versioned());
              node.field("parent", node, f -> f.versioned());
              AutoObject point =
                  version
                      .obj("Point")
                      .field("x", AutoType.ofDec(Decimal4f.class), f -> f.mutable())
                      .field("y", AutoType.ofDec(Decimal4f.class), f -> f.mutable());
              AutoObject edge =
                  version
                      .obj("Edge")
                      .field("from", node, f -> f.versioned())
                      .field("to", node, f -> f.versioned())
                      .field("color", AutoType.string, f -> f.versioned().def("$S", ""))
                      .field("selected", AutoType.bool, f -> f.mutable())
                      .field(
                          "segments",
                          new ListType(
                              version
                                  .obj("Segment")
                                  .field("start", point, f -> f.mutable())
                                  .field("end", point, f -> f.mutable())),
                          f -> f.mutable());
              version
                  .rootObj("Project")
                  .field("style", AutoType.string, f -> f.versioned().def("\"\""))
                  .field("nodes", new ListType(node), f -> f.versioned())
                  .field("edges", new ListType(edge), f -> f.versioned());
            })
        .generate(path);
  }
}
