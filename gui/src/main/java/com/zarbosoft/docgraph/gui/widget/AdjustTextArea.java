package com.zarbosoft.docgraph.gui.widget;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class AdjustTextArea extends TextArea {
  private final TextField ref;
  private ChangeListener<Bounds> lineFixer;

  public AdjustTextArea(TextField ref) {
    this.ref = ref;
    setPrefRowCount(0);
    setWrapText(true);
    ChangeListener<Bounds> keyMirrorBounds =
        (observable, oldValue, newValue) -> {
          if (getPrefRowCount() == 0) {
            setMinHeight(ref.minHeight(-1));
            setPrefHeight(ref.prefHeight(-1));
          } else {
            setMinHeight(-1);
            setPrefHeight(-1);
          }
        };
    setMaxWidth(Double.MAX_VALUE);
    ref.layoutBoundsProperty().addListener(keyMirrorBounds);
    keyMirrorBounds.changed(null, null, null);
  }

  @Override
  protected void layoutChildren() {
    super.layoutChildren();
    if (lineFixer == null) {
      Text child = (Text) lookup(".text");
      lineFixer =
          new ChangeListener<>() {
            @Override
            public void changed(ObservableValue<? extends Bounds> ov, Bounds t, Bounds t1) {
              int lines;
              {
                Text internal = new Text(getText().lines().findFirst().orElse("x"));
                internal.setFont(child.getFont());
                float lineHeight = (float) internal.getLayoutBounds().getHeight();
                lines = (int) (t1.getHeight() / lineHeight);
              }
              setPrefRowCount(lines - 1);
              if (getPrefRowCount() == 0) {
                setMinHeight(ref.minHeight(-1));
                setPrefHeight(ref.prefHeight(-1));
              } else {
                setMinHeight(-1);
                setPrefHeight(-1);
              }
            }
          };
      child.boundsInLocalProperty().addListener(lineFixer);
    }
  }
}
