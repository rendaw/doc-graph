package com.zarbosoft.docgraph.gui.editor;

import com.zarbosoft.automodel.lib.History;
import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.docgraph.gui.State;
import com.zarbosoft.docgraph.gui.icons.dots_horizontal_png;
import com.zarbosoft.docgraph.gui.linkage.LinkJFXProxy;
import com.zarbosoft.docgraph.gui.linkage.ScalarLinkDest;
import com.zarbosoft.docgraph.gui.model.latest.Node;
import com.zarbosoft.docgraph.gui.widget.AdjustTextArea;
import com.zarbosoft.docgraph.gui.widget.Focuser;
import com.zarbosoft.docgraph.gui.widget.Grid;
import com.zarbosoft.docgraph.gui.widget.SunkenButtonWidget;
import com.zarbosoft.docgraph.gui.widget.WidgetHelper;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NodeEditor extends ScrollPane implements Focuser {
  private final VBox box = new WidgetHelper.VBoxPad1();
  private final LinkJFXProxy<String> textProxy;
  private final LinkJFXProxy<String> colorProxy;
  private final LinkJFXProxy<String> imageProxy;
  private final AdjustTextArea textField;
  private final TextField colorField;
  private final TextField imageField;
  private final ScalarLinkDest<String> colorProxy2;

  public NodeEditor(State state, Node node) {
    setContent(box);
    setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
    setFitToWidth(true);

    // Content
    colorField = new TextField();
    colorProxy =
        new LinkJFXProxy<>(
            state, new History.Tuple(node, "color"), colorField.textProperty(), node.color);
    textField = new AdjustTextArea(colorField);
    textProxy =
        new LinkJFXProxy<>(
            state, new History.Tuple(node, "text"), textField.textProperty(), node.text);
    imageField = new TextField();
    imageProxy =
        new LinkJFXProxy<>(
            state, new History.Tuple(node, "image"), imageField.textProperty(), node.image);

    Grid grid = new Grid();
    grid.add("Text", textField);
    grid.add(
        "Image",
        imageField,
        new SunkenButtonWidget(
                new EventHandler<ActionEvent>() {
                  Path lastPath = null;

                  @Override
                  public void handle(ActionEvent e) {
                    final FileChooser fileChooser = new FileChooser();
                    Path currentPath = Paths.get(imageField.getText());
                    if (Files.isRegularFile(currentPath)) {
                      fileChooser.setInitialFileName(currentPath.toString());
                    } else if (lastPath != null) {
                      fileChooser.setInitialFileName(lastPath.toString());
                    } else {
                      fileChooser.setInitialDirectory(state.path.getParent().toFile());
                    }
                    File res = fileChooser.showOpenDialog(NodeEditor.this.getScene().getWindow());
                    if (res == null) return;
                    lastPath = res.toPath();
                    Path prePath = res.toPath();
                    Path path = state.path.getParent().relativize(prePath);
                    state.change(null, c -> node.image.set(c, path.toString()));
                  }
                })
            .setIconText(dots_horizontal_png.data, "File...")
            .widget());
    ColorPicker p = new ColorPicker();
    p.setStyle("-fx-color-label-visible: false");
    grid.add("Border", colorField, p);
    p.setOnAction(
        e ->
            Link.wrap(
                () -> {
                  Color newValue = p.getValue();
                  state.change(
                      null,
                      c ->
                          node.color.set(
                              c,
                              String.format(
                                  "rgb(%d, %d, %d)",
                                  (int) (newValue.getRed() * 256),
                                  (int) (newValue.getGreen() * 256),
                                  (int) (newValue.getBlue() * 256))));
                }));
    colorProxy2 =
        new ScalarLinkDest<>("reverse color", node.color) {
          @Override
          protected void proc(String value) {
            Color c;
            try {
              c = Color.valueOf(node.color.get());
            } catch (Exception e) {
              return;
            }
            p.setValue(c);
          }
        };

    box.getChildren().add(grid);
  }

  public void destroy() {
    textProxy.destroy();
    colorProxy.destroy();
    colorProxy2.destroy();
    imageProxy.destroy();
  }

  @Override
  public void focus() {
    textField.requestFocus();
  }
}
