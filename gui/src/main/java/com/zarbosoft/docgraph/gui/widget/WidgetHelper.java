package com.zarbosoft.docgraph.gui.widget;

import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.docgraph.gui.Global;
import com.zarbosoft.rendaw.common.Pair;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class WidgetHelper {

  public static <R> void drag(
      Node node, MouseButton button, Supplier<R> reference, Consumer<Pair<R, Point2D>> drag) {
    class State {
      boolean set = false;
      R reference = null;
      Point2D start = null;
    }
    State state = new State();
    node.addEventFilter(
        MouseEvent.MOUSE_MOVED,
        e ->
            Link.wrap(
                () -> {
                  if (!e.isMiddleButtonDown()) {
                    state.set = false;
                  }
                }));
    node.addEventHandler(
        MouseEvent.MOUSE_PRESSED,
        e ->
            Link.wrap(
                () -> {
                  if (e.getButton() != button) return;
                  state.set = true;
                  state.reference = reference.get();
                  state.start = new Point2D(e.getSceneX(), e.getSceneY());
                }));
    node.addEventFilter(
        MouseEvent.MOUSE_CLICKED,
        e -> {
          if (state.set) e.consume();
        });
    node.addEventFilter(
        MouseEvent.MOUSE_DRAGGED,
        e ->
            Link.wrap(
                () -> {
                  if (!state.set) return;
                  Point2D diff = new Point2D(e.getSceneX(), e.getSceneY()).subtract(state.start);
                  drag.accept(new Pair<>(state.reference, diff));
                  e.consume();
                }));
    node.addEventFilter(
        MouseEvent.MOUSE_RELEASED,
        e ->
            Link.wrap(
                () -> {
                  if (state.set) {
                    state.set = false;
                    e.consume();
                  }
                }));
  }

  public static class HeaderLabel extends Label {
    public HeaderLabel(String text) {
      super(text);
      fixMin(this);
      setStyle("-fx-font-weight: bold;");
      setPadding(new Insets(8, 0, 3, 0));
    }
  }

  public static class HBoxPad1 extends HBox {
    public HBoxPad1() {
      setPadding(new Insets(Global.WIDGET_PADDING));
      setSpacing(Global.WIDGET_PADDING);
    }
  }

  public static class HBoxSpace1 extends HBox {
    public HBoxSpace1() {
      setSpacing(Global.WIDGET_PADDING);
    }
  }

  public static class VBoxPad1 extends VBox {
    public VBoxPad1() {
      setPadding(new Insets(Global.WIDGET_PADDING));
      setSpacing(Global.WIDGET_PADDING);
    }
  }

  public static class VBoxSpace1 extends VBox {
    public VBoxSpace1() {
      setSpacing(Global.WIDGET_PADDING);
    }
  }

  public static TextField textEdit() {
    return fixMin(new TextField());
  }

  public static Button button() {
    return fixMin(new Button());
  }

  public static <T extends Region> T fixMin(T r) {
    r.layoutBoundsProperty()
        .addListener(
            (observable, oldValue, newValue) -> {
              r.setMinWidth(r.minWidth(-1));
              r.setMinHeight(r.minHeight(-1));
            });
    return r;
  }
}
