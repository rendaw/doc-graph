package com.zarbosoft.docgraph.gui.widget;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

public class Grid extends GridPane {
  int row = 0;

  public Grid() {
    setHgap(3);
    setVgap(3);
  }

  public void add(String label, Node... nodes) {
    Label label1 = new Label(label);
    label1.setMinWidth(Region.USE_PREF_SIZE);
    int col = 0;
    super.add(label1, col++, row);
    for (Node node : nodes) {
      add(node, col++, row);
    }
    row += 1;
  }
}
