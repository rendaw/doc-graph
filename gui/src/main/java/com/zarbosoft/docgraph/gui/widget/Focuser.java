package com.zarbosoft.docgraph.gui.widget;

public interface Focuser {
  public void focus();
}
