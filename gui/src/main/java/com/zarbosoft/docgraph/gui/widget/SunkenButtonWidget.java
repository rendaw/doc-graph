package com.zarbosoft.docgraph.gui.widget;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class SunkenButtonWidget extends ButtonWidget {

  public SunkenButtonWidget(EventHandler<ActionEvent> c) {
    super(c);
    button.getStyleClass().add("sunken-button");
  }
}
