package com.zarbosoft.docgraph.gui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Base64;

import static com.zarbosoft.rendaw.common.Common.substr;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public class DataURI {
  private static final String PROTO = "dataurl";

  private static class StringURLConnection extends URLConnection {
    public StringURLConnection(URL url) {
      super(url);
    }

    @Override
    public void connect() throws IOException {}

    @Override
    public InputStream getInputStream() throws IOException {
      return new ByteArrayInputStream(
          Base64.getUrlDecoder().decode(substr(url.toString(), PROTO.length() + 1)));
    }
  }

  static {
    final URLStreamHandler streamHandler =
        new URLStreamHandler() {
          @Override
          protected URLConnection openConnection(URL url) throws IOException {
            return new StringURLConnection(url);
          }
        };
    URL.setURLStreamHandlerFactory(
        p -> {
          if ("dataurl".equals(p)) {
            return streamHandler;
          }
          return null;
        });
  }

  public static URL format(byte[] data) {
    return uncheck(() -> new URL(PROTO + ":" + Base64.getUrlEncoder().encodeToString(data)));
  }
}
