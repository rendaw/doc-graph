package com.zarbosoft.docgraph.gui;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.zarbosoft.rendaw.common.TemporaryDirectory;
import javafx.scene.image.Image;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public class SVG {
  public static final Cache<Path, Image> svgCache = CacheBuilder.newBuilder().weakValues().build();

  public static Image load(Path path0, int width, int height) {
    Path path = path0.toAbsolutePath();
    return uncheck(
        () ->
            svgCache.get(
                path,
                () ->
                    uncheck(
                        () -> {
                          try (TemporaryDirectory d = new TemporaryDirectory("docgraph-svg")) {
                            Path out = d.path.resolve("out.png");
                            if (new ProcessBuilder(
                                        "inkscape",
                                        path.toString(),
                                        "-o",
                                        out.toString(),
                                        "-w",
                                        Integer.toString(width),
                                        "-h",
                                        Integer.toString(height))
                                    .start()
                                    .onExit()
                                    .get()
                                    .exitValue()
                                != 0) throw new RuntimeException("Failed to convert svg");
                            try (InputStream is = Files.newInputStream(out)) {
                              return new Image(is);
                            }
                          }
                        })));
  }
}
