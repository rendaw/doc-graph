package com.zarbosoft.docgraph.gui.linkage;

import com.zarbosoft.automodel.lib.History;
import com.zarbosoft.automodel.lib.link.AMScalar;
import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.docgraph.gui.State;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.SelectionModel;

public class LinkJFXSelectionProxy<T> extends Link {
  private final SelectionModel<T> property;
  private final ChangeListener<T> listener;
  private final AMScalar<T> main;
  private final History.Tuple unique;

  public LinkJFXSelectionProxy(
      State state, History.Tuple unique, SelectionModel<T> property, AMScalar<T> main) {
    this.unique = unique;
    this.property = property;
    listener =
        (observable, oldValue, newValue) ->
            Link.wrap(
                () -> {
                  mark();
                  state.change(unique, c -> main.set(c, newValue));
                });
    property.selectedItemProperty().addListener(listener);
    this.main = main;
    main.addDest(this);
    main.addSource(this);
    mark();
  }

  @Override
  public String comment() {
    return unique.toString();
  }

  public void destroy() {
    main.removeDest(this);
    main.removeSource(this);
  }

  @Override
  public boolean process() {
    if (main.get() == property.getSelectedItem()) return false;
    property.clearSelection();
    property.select(main.get());
    return true;
  }
}
