package com.zarbosoft.docgraph.gui.widget;

import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class Toolbar extends ToolBar {
  public Toolbar(Orientation orientation) {
    minWidthProperty().bind(prefWidthProperty());
    setOrientation(orientation);
  }

  public Toolbar add(Node child) {
    getItems().add(child);
    return this;
  }

  public Toolbar separator() {
    Separator n = new Separator();
    n.setOrientation(Orientation.VERTICAL);
    getItems().add(n);
    return this;
  }

  public Toolbar gap() {
    Region gap = new Region();
    gap.setMaxWidth(Double.MAX_VALUE);
    gap.setMaxHeight(Double.MAX_VALUE);
    HBox.setHgrow(gap, Priority.ALWAYS);
    VBox.setVgrow(gap, Priority.ALWAYS);
    getItems().add(gap);
    return this;
  }
}
