package com.zarbosoft.docgraph.gui.editor;

import com.zarbosoft.automodel.lib.ChangeStep;
import com.zarbosoft.automodel.lib.History;
import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.docgraph.gui.State;
import com.zarbosoft.docgraph.gui.icons.dots_horizontal_png;
import com.zarbosoft.docgraph.gui.model.latest.Node;
import com.zarbosoft.docgraph.gui.widget.AdjustTextArea;
import com.zarbosoft.docgraph.gui.widget.Focuser;
import com.zarbosoft.docgraph.gui.widget.Grid;
import com.zarbosoft.docgraph.gui.widget.SunkenButtonWidget;
import com.zarbosoft.docgraph.gui.widget.WidgetHelper;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MultiNodeEdit extends ScrollPane implements Focuser {
  private final VBox box = new WidgetHelper.VBoxPad1();
  private final AdjustTextArea textField;
  private final TextField colorField;
  private final TextField imageField;
  private final ColorPicker colorPicker;

  public MultiNodeEdit(State state) {
    setContent(box);
    setUserData(this);
    setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
    setFitToWidth(true);

    // Content
    colorField = new TextField();
    colorField.setText(mostCommon(state.nodeSelection, n -> n.color.get()));
    colorField
        .textProperty()
        .addListener(
            (observableValue, s, t1) ->
                Link.wrap(
                    () -> {
                      setAll(state, new History.Tuple("multi color"), (n, c) -> n.color.set(c, t1));
                      syncColorPicker();
                    }));
    textField = new AdjustTextArea(colorField);
    textField.setText(mostCommon(state.nodeSelection, n -> n.text.get()));
    textField
        .textProperty()
        .addListener(
            (observableValue, s, t1) ->
                Link.wrap(
                    () -> {
                      setAll(state, new History.Tuple("multi text"), (n, c) -> n.text.set(c, t1));
                    }));
    imageField = new TextField();
    imageField.setText(mostCommon(state.nodeSelection, n -> n.image.get()));
    imageField
        .textProperty()
        .addListener(
            (observableValue, s, t1) ->
                Link.wrap(
                    () -> {
                      setAll(state, new History.Tuple("multi image"), (n, c) -> n.image.set(c, t1));
                    }));

    Grid grid = new Grid();
    grid.add("Text", textField);
    grid.add(
        "Image",
        imageField,
        new SunkenButtonWidget(
                new EventHandler<ActionEvent>() {
                  Path lastPath = null;

                  @Override
                  public void handle(ActionEvent e) {
                    final FileChooser fileChooser = new FileChooser();
                    Path currentPath = Paths.get(imageField.getText());
                    if (Files.isRegularFile(currentPath)) {
                      fileChooser.setInitialFileName(currentPath.toString());
                    } else if (lastPath != null) {
                      fileChooser.setInitialFileName(lastPath.toString());
                    } else {
                      fileChooser.setInitialDirectory(state.path.getParent().toFile());
                    }
                    File res =
                        fileChooser.showOpenDialog(MultiNodeEdit.this.getScene().getWindow());
                    if (res == null) return;
                    lastPath = res.toPath();
                    Path prePath = res.toPath();
                    Path path = state.path.getParent().relativize(prePath);
                    imageField.setText(path.toString());
                    setAll(
                        state,
                        new History.Tuple("multi image"),
                        (n, c) -> n.image.set(c, path.toString()));
                  }
                })
            .setIconText(dots_horizontal_png.data, "File...")
            .widget());
    colorPicker = new ColorPicker();
    syncColorPicker();
    colorPicker.setStyle("-fx-color-label-visible: false");
    grid.add("Border", colorField, colorPicker);
    colorPicker.setOnAction(
        e ->
            Link.wrap(
                () -> {
                  Color newValue = colorPicker.getValue();
                  String textValue =
                      String.format(
                          "rgb(%d, %d, %d)",
                          (int) (newValue.getRed() * 256),
                          (int) (newValue.getGreen() * 256),
                          (int) (newValue.getBlue() * 256));
                  setAll(
                      state, new History.Tuple("multi color"), (n, c) -> n.color.set(c, textValue));
                  colorField.setText(textValue);
                }));

    box.getChildren().add(grid);
  }

  private void syncColorPicker() {
    Color c;
    try {
      c = Color.valueOf(colorField.getText());
    } catch (Exception e) {
      return;
    }
    colorPicker.setValue(c);
  }

  private static String mostCommon(List<Node> list, Function<Node, String> acc) {
    return list.stream()
        .map(acc)
        .collect(Collectors.groupingBy(t -> t, Collectors.counting()))
        .entrySet()
        .stream()
        .max(Map.Entry.comparingByValue())
        .map(Map.Entry::getKey)
        .get();
  }

  private static void setAll(
      State state, History.Tuple unique, BiConsumer<Node, ChangeStep> setter) {
    state.change(
        unique,
        c -> {
          for (Node node : state.nodeSelection) {
            setter.accept(node, c);
          }
        });
  }

  @Override
  public void focus() {
    textField.requestFocus();
  }

  public void destroy() {}
}
