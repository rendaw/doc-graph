package com.zarbosoft.docgraph.gui.linkage;

import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.automodel.lib.link.ScalarLinkBase;

public abstract class ScalarLinkDest<T> extends Link {
  public final String comment;
  private final ScalarLinkBase<T> source;

  public ScalarLinkDest(String comment, ScalarLinkBase<T> source) {
    this.comment = comment;
    this.source = source;
    source.addDest(this);
    mark();
  }

  @Override
  public boolean process() {
    proc(source.get());
    return true;
  }

  protected abstract void proc(T value);

  public void destroy() {
    source.removeDest(this);
  }
}
