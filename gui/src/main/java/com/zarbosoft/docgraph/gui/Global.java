package com.zarbosoft.docgraph.gui;

import org.decimal4j.immutable.Decimal4f;

public class Global {
  public static final int MAX_UNDO = 100;
  public static final int WIDGET_PADDING = 6;
  public static final String PAIR_TEXT_KEY = "text";
  public static double UNIT = 30; // ~1cm
  public static double SMALL_UNIT = UNIT / 5;
  public static int IMAGE_SIZE = (int) UNIT;
  public static final double TEXT_HEIGHT = SMALL_UNIT * 2.5;
  public static final double TEXT_PADDING = SMALL_UNIT;
  public static Decimal4f defaultWidth = Decimal4f.valueOf(UNIT * 3);
  public static Decimal4f defaultHeight = Decimal4f.valueOf(UNIT * 2);
  public static Decimal4f defaultPad = Decimal4f.valueOf(UNIT / 2);
  public static double BORDER_THICKNESS = SMALL_UNIT / 2;
  public static double LINE_THICKNESS = SMALL_UNIT;
}
