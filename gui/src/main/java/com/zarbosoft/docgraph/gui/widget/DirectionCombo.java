package com.zarbosoft.docgraph.gui.widget;

import com.zarbosoft.docgraph.gui.model.latest.GraphDirection;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class DirectionCombo {

  public ComboBox<GraphDirection> combo;

  {
    combo = new ComboBox<GraphDirection>();
    combo.setCellFactory(
      new Callback<ListView<GraphDirection>, ListCell<GraphDirection>>() {
        @Override
        public ListCell<GraphDirection> call(ListView<GraphDirection> param) {
          return new ListCell<>() {
            @Override
            protected void updateItem(GraphDirection item, boolean empty) {
              if (item != null) {
                switch (item) {
                  case UP:
                    setText("Up");
                    break;
                  case DOWN:
                    setText("Down");
                    break;
                  case LEFT:
                    setText("Left");
                    break;
                  case RIGHT:
                    setText("Right");
                    break;
                }
              }
              super.updateItem(item, empty);
            }
          };
        }
      });
    combo.setButtonCell(combo.getCellFactory().call(null));
    combo
      .getItems()
      .addAll(GraphDirection.RIGHT, GraphDirection.DOWN, GraphDirection.LEFT, GraphDirection.UP);
    combo.setMaxWidth(Double.MAX_VALUE);
  }
}
