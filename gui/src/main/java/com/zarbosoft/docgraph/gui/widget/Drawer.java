package com.zarbosoft.docgraph.gui.widget;

import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.automodel.lib.link.ScalarLink;
import com.zarbosoft.docgraph.gui.icons.chevron_double_left_png;
import com.zarbosoft.docgraph.gui.icons.chevron_double_right_png;
import com.zarbosoft.docgraph.gui.linkage.ScalarLinkDest;
import javafx.geometry.Orientation;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;

import static com.zarbosoft.docgraph.gui.State.delay;

public class Drawer {
  public final SplitPane split;
  public final HBox box;
  public final Toolbar toolbar;
  public Double storedPosition = null;

  public Drawer(ScalarLink<Boolean> expanded) {
    split = new SplitPane();
    split.getStyleClass().add("drawer");
    box = new HBox();
    toolbar =
        new Toolbar(Orientation.VERTICAL)
            .add(
                new ToolbarButtonWidget(
                        e -> {
                          expanded.set(!expanded.get());
                        })
                    .apply(
                        c -> {
                          c.button.setCursor(Cursor.DEFAULT);
                          new ScalarLinkDest<Boolean>("drawer expand", expanded) {
                            @Override
                            public void proc(Boolean value) {
                              if (value) {
                                c.setIconText(chevron_double_right_png.data, "Hide");
                                Node child = box.getChildren().get(1);
                                delay(
                                    () -> {
                                      ((Focuser) child).focus();
                                    });
                                child.setVisible(true);
                                box.minWidthProperty()
                                    .bind(
                                        toolbar
                                            .widthProperty()
                                            .add(((Region) child).minWidthProperty()));
                                box.maxWidthProperty().unbind();
                                box.setMaxWidth(-1);
                                if (storedPosition == null) split.setDividerPosition(0, 0.7);
                                else split.setDividerPosition(0, storedPosition);
                                toolbar.setCursor(Cursor.W_RESIZE);
                              } else {
                                c.setIconText(chevron_double_left_png.data, "Show");
                                storedPosition = split.getDividerPositions()[0];
                                box.getChildren().get(1).setVisible(false);
                                box.minWidthProperty().bind(toolbar.widthProperty());
                                box.maxWidthProperty().bind(toolbar.widthProperty());
                                toolbar.setCursor(Cursor.DEFAULT);
                              }
                            }
                          };
                        })
                    .widget())
            .gap();
    WidgetHelper.drag(
        toolbar,
        MouseButton.PRIMARY,
        () -> split.getDividerPositions()[0] * split.getWidth(),
        p -> {
          split.setDividerPosition(0, (p.first + p.second.getX()) / split.getWidth());
        });
    box.getChildren().add(toolbar);

    toolbar.setBorder(
        new Border(
            new BorderStroke(
                Color.DARKGRAY,
                BorderStrokeStyle.SOLID,
                CornerRadii.EMPTY,
                new BorderWidths(0, 1, 0, 0))));

    split.getItems().add(box);

    box.addEventHandler(
        KeyEvent.KEY_PRESSED,
        e ->
            Link.wrap(
                () -> {
                  if (e.getCode() == KeyCode.ESCAPE) {
                    e.consume();
                    expanded.set(false);
                  }
                }));
  }

  public void setLeft(Node left) {
    split.getItems().add(0, left);
  }

  public void setRight(Region right) {
    HBox.setHgrow(right, Priority.ALWAYS);
    box.getChildren().add(right);
  }

  public Parent widget() {
    return split;
  }
}
