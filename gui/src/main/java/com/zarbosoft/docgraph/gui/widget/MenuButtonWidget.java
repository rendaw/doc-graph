package com.zarbosoft.docgraph.gui.widget;

import com.zarbosoft.automodel.lib.link.Link;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.ByteArrayInputStream;
import java.util.function.Consumer;

import static com.zarbosoft.docgraph.gui.widget.Icons.icons;

public class MenuButtonWidget {

  protected final MenuButton button = new MenuButton();

  public MenuButtonWidget() {
    button.setFocusTraversable(false);
    button.getStyleClass().add("sunken-button");
  }

  public Node widget() {
    return button;
  }

  public MenuButtonWidget setIconText(byte[] icon, String text) {
    button.setGraphic(
        new ImageView(icons.computeIfAbsent(icon, k -> new Image(new ByteArrayInputStream(k)))));
    button.setTooltip(new Tooltip(text));
    return this;
  }

  public MenuButtonWidget add(MenuItem item) {
    button.getItems().add(item);
    return this;
  }

  public MenuButtonWidget add(byte[] icon, String text, EventHandler<ActionEvent> action) {
    MenuItem item = new MenuItem();
    item.setText(text);
    if (icon != null)
      item.setGraphic(
          new ImageView(icons.computeIfAbsent(icon, k -> new Image(new ByteArrayInputStream(k)))));
    item.setOnAction(e -> Link.wrap(() -> action.handle(e)));
    button.getItems().add(item);
    return this;
  }

  public MenuButtonWidget apply(Consumer<MenuButtonWidget> c) {
    c.accept(this);
    return this;
  }
}
