package com.zarbosoft.docgraph.gui.linkage;

import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.automodel.lib.link.ListLinkBase;

import java.util.List;

public abstract class SimpleListLinkDest<T> extends Link {
  public final String comment;
  private final ListLinkBase<T> source;

  public SimpleListLinkDest(String comment, ListLinkBase<T> source) {
    this.comment = comment;
    this.source = source;
    source.addDest(this);
  }

  @Override
  public final String comment() {
    return comment;
  }

  @Override
  public final boolean process() {
    proc(source);
    return true;
  }

  protected abstract void proc(List<T> value);

  public void destroy() {
    source.removeDest(this);
  }
}
