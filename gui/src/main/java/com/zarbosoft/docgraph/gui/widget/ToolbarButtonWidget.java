package com.zarbosoft.docgraph.gui.widget;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ToolbarButtonWidget extends ButtonWidget{

  public ToolbarButtonWidget(EventHandler<ActionEvent> c) {
    super(c);
    button.setFocusTraversable(false);
    button.getStyleClass().add("toolbar-button");
  }
}
