package com.zarbosoft.docgraph.gui.widget;

import com.zarbosoft.automodel.lib.link.Link;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.ByteArrayInputStream;
import java.util.function.Consumer;

import static com.zarbosoft.docgraph.gui.widget.Icons.icons;

public class ButtonWidget {
  protected final Button button = new Button();

  public ButtonWidget(EventHandler<ActionEvent> c) {
    this.button.setOnAction(e -> Link.wrap(() -> c.handle(e)));
  }

  public Node widget() {
    return button;
  }

  public ButtonWidget setIconText(byte[] icon, String text) {
    button.setGraphic(
        new ImageView(icons.computeIfAbsent(icon, k -> new Image(new ByteArrayInputStream(k)))));
    button.setTooltip(new Tooltip(text));
    return this;
  }

  public ButtonWidget apply(Consumer<ButtonWidget> c) {
    c.accept(this);
    return this;
  }

  public void enable(boolean b) {
    button.setDisable(!b);
  }
}
