package com.zarbosoft.docgraph.gui.widget;

import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.docgraph.gui.Global;
import com.zarbosoft.docgraph.gui.Main;
import com.zarbosoft.docgraph.gui.SVG;
import com.zarbosoft.docgraph.gui.State;
import com.zarbosoft.docgraph.gui.linkage.ScalarLinkDest;
import com.zarbosoft.docgraph.gui.linkage.SimpleListLinkDest;
import com.zarbosoft.docgraph.gui.model.latest.Edge;
import com.zarbosoft.docgraph.gui.model.latest.Node;
import com.zarbosoft.docgraph.gui.model.latest.SelectState;
import com.zarbosoft.rendaw.common.Pair;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class NodeWidget {
  public final Node node;
  private final Link geomListener;
  private final ScalarLinkDest<SelectState> selectedListener;
  private final State state;
  private final ScalarLinkDest<String> textListener;
  private final ScalarLinkDest<String> colorListener;
  private final ScalarLinkDest<String> imageListener;
  private final Runnable cleanMirrorChildren;
  private final SimpleListLinkDest<Node> nodeHasChildren;
  public Group group = new Group();
  public Group rectGroup = new Group();
  public Rectangle rectangle = new Rectangle();
  public ImageView image = new ImageView();
  public VBox contents = new WidgetHelper.VBoxSpace1();
  public Label text = new Label();
  public Group children = new Group();

  public NodeWidget(State state, Node node) {
    this.state = state;
    this.node = node;
    this.text.getStyleClass().add("node-label");
    // this.text.setTextOverrun(OverrunStyle.ELLIPSIS);
    this.text.setFont(Font.font("sans-serif", FontWeight.SEMI_BOLD, Global.TEXT_HEIGHT));
    this.text.setTextAlignment(TextAlignment.CENTER);
    this.contents.setAlignment(Pos.CENTER);
    DoubleBinding widthBinding = rectangle.widthProperty().subtract(Global.SMALL_UNIT * 2);
    this.contents.maxWidthProperty().bind(widthBinding);
    this.contents.minWidthProperty().bind(widthBinding);
    this.contents.setLayoutX(Global.SMALL_UNIT);
    this.contents.getChildren().addAll(this.text);

    this.rectangle.setStrokeWidth(Global.BORDER_THICKNESS);
    nodeHasChildren =
        new SimpleListLinkDest<>("node has children", node.children) {
          @Override
          protected void proc(List<Node> value) {
            if (value.isEmpty()) {
              // text.setWrapText(true);
              DoubleBinding heightBinding =
                  rectangle.heightProperty().subtract(Global.SMALL_UNIT * 2);
              contents.maxHeightProperty().bind(heightBinding);
              contents.minHeightProperty().bind(heightBinding);
              contents.prefHeightProperty().bind(heightBinding);
              contents.setLayoutY(Global.SMALL_UNIT);
            } else {
              // text.setWrapText(false);
              ObjectBinding<Double> heightBinding =
                  Bindings.createObjectBinding(
                      () -> Global.TEXT_HEIGHT + (image.getImage() == null ? 0 : Global.IMAGE_SIZE),
                      image.imageProperty());
              contents.maxHeightProperty().bind(heightBinding);
              contents.minHeightProperty().bind(heightBinding);
              contents.prefHeightProperty().bind(heightBinding);
              contents.setLayoutY(Global.SMALL_UNIT + Global.TEXT_PADDING);
            }
          }
        };
    nodeHasChildren.addDest(state.graphLayout);
    selectedListener =
        new ScalarLinkDest<>("node widget selected", node.selected) {
          @Override
          protected void proc(SelectState value) {
            updateColor();
          }
        };
    this.rectangle.setArcHeight(Global.SMALL_UNIT * 2);
    this.rectangle.setArcWidth(Global.SMALL_UNIT * 2);
    this.rectangle.setFill(Color.TRANSPARENT);
    this.rectGroup.getChildren().addAll(this.rectangle, this.contents);
    this.group.getChildren().addAll(rectGroup, children);
    this.group.setUserData(this);

    textListener =
        new ScalarLinkDest<>("node widget text", node.text) {
          @Override
          protected void proc(String value) {
            text.setText(value);
          }
        };
    colorListener =
        new ScalarLinkDest<>("node widget color", node.color) {
          @Override
          protected void proc(String value) {
            updateColor();
          }
        };
    imageListener =
        new ScalarLinkDest<>("node widget image", node.image) {
          @Override
          protected void proc(String value) {
            Path imagePath = state.path.getParent().resolve(value);
            if (!Files.isRegularFile(imagePath)) {
              contents.getChildren().remove(image);
              image.setImage(null);
            } else {
              if (imagePath.toString().endsWith(".svg")) {
                Main.background.execute(
                    () -> {
                      Image loaded = SVG.load(imagePath, Global.IMAGE_SIZE, Global.IMAGE_SIZE);
                      Platform.runLater(
                          () -> {
                            image.setImage(loaded);
                          });
                    });
              } else image.setImage(new Image(imagePath.toString()));
              if (!contents.getChildren().contains(image)) contents.getChildren().add(0, image);
            }
          }
        };
    imageListener.addDest(state.graphLayout);
    cleanMirrorChildren =
        node.children.mirror(
            children.getChildren(),
            n -> new NodeWidget(state, n).widget(),
            n -> ((NodeWidget) n.getUserData()).destroy(),
            (at, count) -> {});

    geomListener =
        new Link() {
          {
            mark();
          }

          @Override
          public boolean process() {
            double width = node.width.get().doubleValue();
            rectangle.setWidth(width);
            double height = node.height.get().doubleValue();
            rectangle.setHeight(height);
            rectGroup.setLayoutX(node.x.get().doubleValue());
            rectGroup.setLayoutY(node.y.get().doubleValue());
            if (node.justCreated.get()) {
              scrollTo(state, node);
              node.justCreated.set(false);
            }
            return true;
          }

          @Override
          public String comment() {
            return "node geom";
          }
        }.addSource(state.graphLayout);

    this.group.addEventHandler(
        MouseEvent.MOUSE_CLICKED,
        e ->
            Link.wrap(
                () -> {
                  if (e.getButton() == MouseButton.PRIMARY) {
                    e.consume();
                    if (e.isControlDown() || e.isShiftDown()) {
                      state.selectNodeToggle(node);
                    } else {
                      state.select(node);
                    }
                  } else if (e.getButton() == MouseButton.SECONDARY) {
                    if (e.isControlDown()) {
                      state.toggleInGroup(state.nodeSelection, node);
                    } else {
                      if (state.nodeSelection.size() == 1) {
                        e.consume();
                        Pair<Integer, Edge> connected =
                            state.isConnected(((List<Node>) state.nodeSelection).get(0), node);
                        if (connected != null) {
                          state.disconnect(connected);
                        } else {
                          state.connect(((List<Node>) state.nodeSelection).get(0), node);
                        }
                      }
                    }
                  }
                }));
  }

  public javafx.scene.Node widget() {
    return group;
  }

  private void updateColor() {
    SelectState value = node.selected.get();
    switch (value) {
      case NONE:
        {
          Color c = Color.BLACK;
          try {
            c = Color.valueOf(node.color.get());
          } catch (Exception e) {
          }
          rectangle.setStroke(c);
        }
        break;
      case PRIMARY:
        rectangle.setStroke(Color.BLUEVIOLET);
        break;
      case SECONDARY:
        rectangle.setStroke(Color.YELLOWGREEN);
        break;
    }
  }

  public static void scrollTo(State state, Node node) {
    state.scroll.set(
        new Point2D(
            node.x.get().doubleValue() + node.width.get().doubleValue() / 2,
            node.y.get().doubleValue() + node.height.get().doubleValue() / 2));
  }

  public void destroy() {
    cleanMirrorChildren.run();
    geomListener.removeSource(state.graphLayout);
    selectedListener.destroy();
    textListener.destroy();
    imageListener.removeDest(state.graphLayout);
    imageListener.destroy();
    colorListener.destroy();
    nodeHasChildren.removeDest(state.graphLayout);
    nodeHasChildren.destroy();
  }
}
