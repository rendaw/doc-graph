package com.zarbosoft.docgraph.gui.linkage;

import com.zarbosoft.automodel.lib.ChangeStep;
import com.zarbosoft.automodel.lib.History;
import com.zarbosoft.automodel.lib.link.AMScalar;
import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.docgraph.gui.State;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;

import java.util.Set;

public class LinkJFXProxy<T> extends Link {
  private final Property<T> property;
  private final ChangeListener<T> listener;
  private final AMScalar<T> main;
  private final History.Tuple unique;

  public LinkJFXProxy(State state, History.Tuple unique, Property<T> property, AMScalar<T> main) {
    this.unique = unique;
    this.property = property;
    listener = (observable, oldValue, newValue) -> Link.wrap(() -> {
      mark();
      state.change(unique, c -> {
        main.set(c,newValue);
        processHook(c,oldValue,newValue);
      });
    });
    property.addListener(listener);
    this.main = main;
    main.addDest(this);
    main.addSource(this);
    mark();
  }

  @Override
  public String comment() {
    return unique.toString();
  }

  public void destroy() {
    main.removeDest(this);
    main.removeSource(this);
  }

  @Override
  public boolean process() {
    if (main.get() == property.getValue()) return false;
    property.setValue(main.get());
    return true;
  }
  protected void processHook(ChangeStep c, T oldValue, T value) {

  }
}
