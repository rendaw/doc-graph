package com.zarbosoft.docgraph.gui.widget;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Bounds;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.Region;

public class Tabs extends TabPane implements Focuser {
  private ChangeListener<? super Bounds> listener;

  public Tabs() {
    setStyle("-fx-open-tab-animation: none; -fx-close-tab-animation: none;");
    setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
    setTabDragPolicy(TabPane.TabDragPolicy.FIXED);
    getSelectionModel()
        .selectedItemProperty()
        .addListener(
            (observable, oldValue, newValue) -> {
              if (oldValue != null && listener != null)
                oldValue.getContent().layoutBoundsProperty().removeListener(listener);
              listener = null;
              if (newValue != null)
                newValue
                    .getContent()
                    .layoutBoundsProperty()
                    .addListener(
                        listener =
                            (observable1, oldValue1, newValue1) -> {
                              setMinWidth(newValue.getContent().minWidth(-1));
                            });
            });
  }

  public void add(String name, Region content) {
    Tab e = new Tab(name, content);
    getTabs().add(e);
    getSelectionModel().select(e);
  }

  @Override
  public void focus() {
    ((Focuser) getSelectionModel().getSelectedItem().getContent()).focus();
  }
}
