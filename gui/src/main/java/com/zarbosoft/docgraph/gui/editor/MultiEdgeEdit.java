package com.zarbosoft.docgraph.gui.editor;

import com.zarbosoft.automodel.lib.ChangeStep;
import com.zarbosoft.automodel.lib.History;
import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.docgraph.gui.State;
import com.zarbosoft.docgraph.gui.model.latest.Edge;
import com.zarbosoft.docgraph.gui.widget.Focuser;
import com.zarbosoft.docgraph.gui.widget.Grid;
import com.zarbosoft.docgraph.gui.widget.WidgetHelper;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MultiEdgeEdit extends ScrollPane implements Focuser {
  private final VBox box = new WidgetHelper.VBoxPad1();
  private final TextField colorField;
  private final ColorPicker colorPicker;

  public MultiEdgeEdit(State state) {
    setContent(box);
    setUserData(this);
    setHbarPolicy(ScrollBarPolicy.NEVER);
    setFitToWidth(true);

    // Content
    colorField = new TextField();
    colorField.setText(mostCommon(state.edgeSelection, e -> e.color.get()));
    colorField
        .textProperty()
        .addListener(
            (observableValue, s, t1) ->
                Link.wrap(
                    () -> {
                      System.out.format("setall me %s\n", t1);
                      setAll(state, new History.Tuple("multi color"), (e, c) -> e.color.set(c, t1));
                      syncColorPicker();
                    }));

    Grid grid = new Grid();
    colorPicker = new ColorPicker();
    syncColorPicker();
    colorPicker.setStyle("-fx-color-label-visible: false");
    grid.add("Border", colorField, colorPicker);
    colorPicker.setOnAction(
        e ->
            Link.wrap(
                () -> {
                  Color newValue = colorPicker.getValue();
                  String textValue =
                      String.format(
                          "rgb(%d, %d, %d)",
                          (int) (newValue.getRed() * 256),
                          (int) (newValue.getGreen() * 256),
                          (int) (newValue.getBlue() * 256));
                  colorField.setText(textValue);
                  setAll(state, null, (e2, c) -> e2.color.set(c, textValue));
                }));

    box.getChildren().add(grid);
  }

  private static String mostCommon(List<Edge> list, Function<Edge, String> acc) {
    return list.stream()
        .map(acc)
        .collect(Collectors.groupingBy(t -> t, Collectors.counting()))
        .entrySet()
        .stream()
        .max(Map.Entry.comparingByValue())
        .map(Map.Entry::getKey)
        .get();
  }

  private static void setAll(
      State state, History.Tuple unique, BiConsumer<Edge, ChangeStep> setter) {
    state.change(
        unique,
        c -> {
          for (Edge edge : state.edgeSelection) {
            setter.accept(edge, c);
          }
        });
  }

  private void syncColorPicker() {
    do {
      Color c;
      try {
        c = Color.valueOf(colorField.getText());
      } catch (Exception e) {
        break;
      }
      colorPicker.setValue(c);
    } while (false);
  }

  @Override
  public void focus() {
    colorField.requestFocus();
  }

  public void destroy() {}
}
