package com.zarbosoft.docgraph.gui;

import com.google.common.collect.ImmutableList;
import com.zarbosoft.automodel.lib.Logger;
import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.automodel.lib.link.ScalarLink;
import com.zarbosoft.automodel.lib.link.ScalarMapLink;
import com.zarbosoft.docgraph.core.Graph;
import com.zarbosoft.docgraph.core.render.Render;
import com.zarbosoft.docgraph.core.render.error.DefError;
import com.zarbosoft.docgraph.gui.editor.EdgeEditor;
import com.zarbosoft.docgraph.gui.editor.MultiEdgeEdit;
import com.zarbosoft.docgraph.gui.editor.MultiNodeEdit;
import com.zarbosoft.docgraph.gui.editor.NodeEditor;
import com.zarbosoft.docgraph.gui.icons.appicon64_png;
import com.zarbosoft.docgraph.gui.icons.camera_png;
import com.zarbosoft.docgraph.gui.icons.center_png;
import com.zarbosoft.docgraph.gui.icons.content_save_png;
import com.zarbosoft.docgraph.gui.icons.create_after_png;
import com.zarbosoft.docgraph.gui.icons.create_before_png;
import com.zarbosoft.docgraph.gui.icons.create_nested_png;
import com.zarbosoft.docgraph.gui.icons.create_parallel_png;
import com.zarbosoft.docgraph.gui.icons.delete_png;
import com.zarbosoft.docgraph.gui.icons.group_png;
import com.zarbosoft.docgraph.gui.icons.link_off_png;
import com.zarbosoft.docgraph.gui.icons.link_png;
import com.zarbosoft.docgraph.gui.icons.ungroup_png;
import com.zarbosoft.docgraph.gui.linkage.ScalarLinkDest;
import com.zarbosoft.docgraph.gui.linkage.SimpleListLinkDest;
import com.zarbosoft.docgraph.gui.model.ModelVersions;
import com.zarbosoft.docgraph.gui.model.latest.Edge;
import com.zarbosoft.docgraph.gui.model.latest.Model;
import com.zarbosoft.docgraph.gui.model.latest.Node;
import com.zarbosoft.docgraph.gui.model.latest.Project;
import com.zarbosoft.docgraph.gui.widget.Canvas;
import com.zarbosoft.docgraph.gui.widget.Drawer;
import com.zarbosoft.docgraph.gui.widget.EdgeWidget;
import com.zarbosoft.docgraph.gui.widget.NodeWidget;
import com.zarbosoft.docgraph.gui.widget.Tabs;
import com.zarbosoft.docgraph.gui.widget.Toolbar;
import com.zarbosoft.docgraph.gui.widget.ToolbarButtonWidget;
import com.zarbosoft.rendaw.common.Pair;
import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.zarbosoft.docgraph.gui.State.createDefaultNode;
import static com.zarbosoft.rendaw.common.Common.firstOpt;

public class Main extends Application {
  public static final ThreadPoolExecutor background =
      new ThreadPoolExecutor(1, 1, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<>());
  private static Path lastRenderPath;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    Link.wrap(
        () -> {
          Path path;
          if (getParameters().getUnnamed().size() < 1) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Create or open docgraph...");
            fileChooser.setSelectedExtensionFilter(
                new FileChooser.ExtensionFilter("Docgraph (.dg.json)", "*.dg.json"));
            File result = fileChooser.showSaveDialog(null);
            if (result == null) {
              return;
            }
            path = result.toPath();
          } else {
            path = Paths.get(getParameters().getUnnamed().get(0));
          }
          Logger.logger = new Logger.Terminal();
          Path absPath = path.toAbsolutePath();
          final Model model = ModelVersions.create(Global.MAX_UNDO);
          final Project project = (Project) model.root;
          State state = new State(absPath, model, project);

          primaryStage.setTitle(absPath.toString());
          new ScalarLink<Boolean>(false) {
            @Override
            public boolean process() {
              if (state.dirty.get()) primaryStage.setTitle("*" + path);
              else primaryStage.setTitle(path.toString());
              return true;
            }

            @Override
            public String comment() {
              return "title dirty";
            }
          }.addSource(state.dirty);
          primaryStage.getIcons().add(new Image(new ByteArrayInputStream(appicon64_png.data)));
          primaryStage.setWidth(1024);
          primaryStage.setHeight(768);

          primaryStage.setOnCloseRequest(
              e -> {
                if (state.dirty.get()) {
                  if (!confirmClose(primaryStage).isPresent()) {
                    e.consume();
                    return;
                  }
                }
                background.shutdown();
              });

          Canvas canvas = new Canvas(state);
          VBox.setVgrow(canvas.widget(), Priority.ALWAYS);
          new ScalarMapLink<State.GeomResult, Void>(state.graphLayout, null) {
            @Override
            public String comment() {
              return "canvas recenter";
            }

            @Override
            public Void proc(State.GeomResult value) {
              canvas.setCenter(state, value.center);
              return null;
            }
          }.addDest(state.scroll);

          Toolbar toolbar =
              new Toolbar(Orientation.HORIZONTAL)
                  .add(
                      new ToolbarButtonWidget(
                              e ->
                                  state.create(
                                      firstOpt((List<Node>) state.nodeSelection).orElse(null)))
                          .setIconText(create_nested_png.data, "Create (n)")
                          .widget())
                  .add(
                      new ToolbarButtonWidget(
                              e -> state.createBefore(((List<Node>) state.nodeSelection).get(0)))
                          .setIconText(create_before_png.data, "Create before (b)")
                          .apply(
                              b ->
                                  new SimpleListLinkDest<>(
                                      "create before enabled", state.nodeSelection) {
                                    @Override
                                    public void proc(List<Node> value) {
                                      b.enable(value.size() == 1);
                                    }
                                  })
                          .widget())
                  .add(
                      new ToolbarButtonWidget(
                              e -> state.createAfter(((List<Node>) state.nodeSelection).get(0)))
                          .setIconText(create_after_png.data, "Create after (a)")
                          .apply(
                              b ->
                                  new SimpleListLinkDest<>(
                                      "create after enabled", state.nodeSelection) {
                                    @Override
                                    public void proc(List<Node> value) {
                                      b.enable(value.size() == 1);
                                    }
                                  })
                          .widget())
                  .add(
                      new ToolbarButtonWidget(
                              e ->
                                  state.createParallel(
                                      state.maxOneBefore.get(),
                                      ((List<Node>) state.nodeSelection).get(0)))
                          .setIconText(create_parallel_png.data, "Create parallel (p)")
                          .apply(
                              b ->
                                  new ScalarLinkDest<>(
                                      "create parallel enabled", state.maxOneBefore) {
                                    @Override
                                    public void proc(Optional<Node> value) {
                                      b.enable(value != null);
                                    }
                                  })
                          .widget())
                  .separator()
                  .add(
                      new ToolbarButtonWidget(
                              e -> {
                                if (state.connected.get() == null)
                                  state.connect(
                                      ((List<Node>) state.nodeSelection).get(0),
                                      ((List<Node>) state.nodeSelection).get(1));
                                else state.disconnect(state.connected.get());
                              })
                          .apply(
                              b ->
                                  new ScalarLinkDest<>(
                                      "connect disconnect enabled", state.connected) {
                                    @Override
                                    public void proc(Pair<Integer, Edge> value) {
                                      b.enable(((List<Node>) state.nodeSelection).size() == 2);
                                      if (value == null)
                                        b.setIconText(link_png.data, "Connect (right click)");
                                      else
                                        b.setIconText(
                                            link_off_png.data, "Disconnect (right click)");
                                    }
                                  })
                          .widget())
                  .add(
                      new ToolbarButtonWidget(
                              e -> {
                                if (state.nodeSelection.size() == 1) {
                                  state.ungroup(((List<Node>) state.nodeSelection).get(0));
                                } else {
                                  state.group(state.nodeSelection);
                                }
                              })
                          .apply(
                              b ->
                                  new SimpleListLinkDest<>(
                                      "group ungroup enabled", state.nodeSelection) {
                                    @Override
                                    public void proc(List<Node> value) {
                                      if (value.size() == 1 && value.get(0).children.size() >= 1) {
                                        b.enable(true);
                                        b.setIconText(ungroup_png.data, "Ungroup");
                                      } else if (value.size() >= 2) {
                                        b.enable(true);
                                        b.setIconText(group_png.data, "Group");
                                      } else {
                                        b.setIconText(group_png.data, "Group");
                                        b.enable(false);
                                      }
                                    }
                                  })
                          .widget())
                  .separator()
                  .add(
                      new ToolbarButtonWidget(e -> state.delete(state.nodeSelection))
                          .setIconText(delete_png.data, "Delete (del)")
                          .apply(
                              b ->
                                  new SimpleListLinkDest<>("delete enabled", state.nodeSelection) {
                                    @Override
                                    public void proc(List<Node> value) {
                                      b.enable(!value.isEmpty());
                                    }
                                  })
                          .widget())
                  .gap()
                  .separator()
                  .add(
                      new ToolbarButtonWidget(
                              e -> {
                                state.zoom.set(0);
                                state.scroll.set(canvas.center);
                              })
                          .setIconText(center_png.data, "Reset view")
                          .widget());

          VBox canvasLayout = new VBox();
          canvasLayout.getChildren().addAll(toolbar, canvas.widget());

          Drawer layout = new Drawer(state.drawerExpanded);
          layout
              .toolbar
              .gap()
              .add(
                  new ToolbarButtonWidget(
                          e -> {
                            state.save();
                            final FileChooser fileChooser = new FileChooser();
                            fileChooser
                                .getExtensionFilters()
                                .add(new FileChooser.ExtensionFilter("SVG files (*.svg)", "*.svg"));
                            if (lastRenderPath != null) {
                              fileChooser.setInitialFileName(lastRenderPath.toString());
                            } else {
                              fileChooser.setInitialDirectory(state.path.getParent().toFile());
                            }
                            File res =
                                fileChooser.showSaveDialog(layout.widget().getScene().getWindow());
                            if (res == null) return;
                            lastRenderPath = res.toPath();
                            List<DefError> errors = Render.render(state.path, res.toPath());
                            if (!errors.isEmpty()) {
                              Alert alert = new Alert(Alert.AlertType.ERROR);
                              alert.initOwner(primaryStage);
                              alert.initModality(Modality.WINDOW_MODAL);
                              alert.setTitle("Error rendering");
                              alert.setHeaderText("There was an error rendering the graph.");
                              alert.setContentText(
                                  errors.stream()
                                      .map(e1 -> e1.toString())
                                      .collect(Collectors.joining("\n")));
                              alert.show();
                            }
                          })
                      .setIconText(camera_png.data, "Render (ctrl + e)")
                      .widget())
              .add(
                  new ToolbarButtonWidget(e -> state.save())
                      .setIconText(content_save_png.data, "Save (ctrl + s)")
                      .apply(
                          c -> {
                            new ScalarLinkDest<Boolean>("save enabled", state.dirty) {
                              @Override
                              protected void proc(Boolean value) {
                                c.enable(value);
                              }
                            }.addSource(state.dirty);
                          })
                      .widget());
          layout.setLeft(canvasLayout);

          state.tabs = new Tabs();
          layout.setRight(state.tabs);

          Scene scene = new Scene(layout.widget());
          scene.getStylesheets().add(DataURI.format(style_css.data).toString());
          primaryStage.setScene(scene);
          primaryStage.show();

          // Additional linkage
          scene.addEventFilter(
              KeyEvent.KEY_PRESSED,
              e ->
                  Link.wrap(
                      () -> {
                        switch (e.getCode()) {
                          case Z:
                            {
                              if (e.isControlDown()) {
                                e.consume();
                                if (e.isShiftDown()) state.redo();
                                else state.undo();
                              }
                            }
                            break;
                          case Y:
                            {
                              if (e.isControlDown()) {
                                e.consume();
                                state.redo();
                              }
                            }
                            break;
                          case ESCAPE:
                            {
                              if (!state.drawerExpanded.get()) {
                                state.deselect();
                              } else {
                                state.drawerExpanded.set(false);
                              }
                            }
                            break;
                          case S:
                            {
                              if (e.isControlDown()) {
                                e.consume();
                                state.save();
                              }
                            }
                            break;
                          default:
                            return;
                        }
                        e.consume();
                      }));
          canvas.addKeyHandler(
              e ->
                  Link.wrap(
                      () -> {
                        switch (e.getCode()) {
                          case B: // before
                            {
                              e.consume();
                              if (state.nodeSelection.size() == 1) {
                                state.createBefore(state.nodeSelection.get(0));
                              }
                            }
                            break;
                          case A: // after
                            {
                              e.consume();
                              if (state.nodeSelection.size() == 1) {
                                state.createAfter(state.nodeSelection.get(0));
                              }
                            }
                            break;
                          case P: // parallel
                            {
                              e.consume();
                              if (state.maxOneBefore.get() != null) {
                                state.createParallel(
                                    state.maxOneBefore.get(), state.nodeSelection.get(0));
                              }
                            }
                            break;
                          case N: // nested
                            {
                              e.consume();
                              state.create(firstOpt(state.nodeSelection).orElse(null));
                            }
                            break;
                          case E: // edit
                            {
                              e.consume();
                              if (state.nodeSelection.size() > 0) {
                                state.editNodes(state.nodeSelection);
                              } else if (state.edgeSelection.size() > 0) {
                                state.editEdges(state.edgeSelection);
                              }
                            }
                            break;
                          case DELETE:
                            {
                              e.consume();
                              if (state.nodeSelection.size() > 0) {
                                state.delete(state.nodeSelection);
                              }
                            }
                            break;
                          case LEFT:
                            {
                              e.consume();
                              if (state.nodeSelection.size() >= 1) {
                                switch (state.getParentDirection(state.nodeSelection.get(0))) {
                                  case LEFT:
                                    state.selectAfter(state.nodeSelection.get(0));
                                    NodeWidget.scrollTo(state, state.nodeSelection.get(0));
                                    break;
                                  case RIGHT:
                                    state.selectBefore(state.nodeSelection.get(0));
                                    NodeWidget.scrollTo(state, state.nodeSelection.get(0));
                                    break;
                                }
                              }
                            }
                            break;
                          case RIGHT:
                            {
                              e.consume();
                              if (state.nodeSelection.size() >= 1) {
                                switch (state.getParentDirection(state.nodeSelection.get(0))) {
                                  case LEFT:
                                    state.selectBefore(state.nodeSelection.get(0));
                                    NodeWidget.scrollTo(state, state.nodeSelection.get(0));
                                    break;
                                  case RIGHT:
                                    state.selectAfter(state.nodeSelection.get(0));
                                    NodeWidget.scrollTo(state, state.nodeSelection.get(0));
                                    break;
                                }
                              }
                            }
                            break;
                          case UP:
                            {
                              e.consume();
                              if (state.nodeSelection.size() >= 1) {
                                switch (state.getParentDirection(state.nodeSelection.get(0))) {
                                  case UP:
                                    state.selectAfter(state.nodeSelection.get(0));
                                    NodeWidget.scrollTo(state, state.nodeSelection.get(0));
                                    break;
                                  case DOWN:
                                    state.selectBefore(state.nodeSelection.get(0));
                                    NodeWidget.scrollTo(state, state.nodeSelection.get(0));
                                    break;
                                }
                              }
                            }
                            break;
                          case DOWN:
                            {
                              e.consume();
                              if (state.nodeSelection.size() >= 1) {
                                switch (state.getParentDirection(state.nodeSelection.get(0))) {
                                  case UP:
                                    state.selectBefore(state.nodeSelection.get(0));
                                    NodeWidget.scrollTo(state, state.nodeSelection.get(0));
                                    break;
                                  case DOWN:
                                    state.selectAfter(state.nodeSelection.get(0));
                                    NodeWidget.scrollTo(state, state.nodeSelection.get(0));
                                    break;
                                }
                              }
                            }
                            break;
                          case Q:
                            {
                              if (e.isControlDown()) {
                                e.consume();
                                if (confirmClose(primaryStage).isPresent()) {
                                  primaryStage.close();
                                }
                              }
                            }
                            break;
                          default:
                            return;
                        }
                        e.consume();
                      }));
          new SimpleListLinkDest<>("node select editor", state.nodeSelection) {
            @Override
            public void proc(List<Node> list) {
              if (list.size() <= 1) {
                Iterator<Tab> i = state.tabs.getTabs().iterator();
                while (i.hasNext()) {
                  Tab n = i.next();
                  Object u = n.getContent();
                  if (u == null) continue;
                  if (u.getClass() == NodeEditor.class || u.getClass() == MultiNodeEdit.class) {
                    if (u.getClass() == NodeEditor.class) ((NodeEditor) u).destroy();
                    if (u.getClass() == MultiNodeEdit.class) ((MultiNodeEdit) u).destroy();
                    i.remove();
                    break;
                  }
                }

                if (list.size() == 1) {
                  state.tabs.add("Node", new NodeEditor(state, list.get(0)));
                }
              } else if (list.size() > 1) {
                boolean created = false;
                Iterator<Tab> i = state.tabs.getTabs().iterator();
                while (i.hasNext()) {
                  Tab n = i.next();
                  Object u = n.getContent();
                  if (u == null) continue;
                  if (u.getClass() == NodeEditor.class) {
                    ((NodeEditor) u).destroy();
                    i.remove();
                    break;
                  } else if (u.getClass() == MultiNodeEdit.class) {
                    created = true;
                  }
                }
                if (!created) {
                  state.tabs.add("Multi", new MultiNodeEdit(state));
                }
              }
            }
          };
          new SimpleListLinkDest<>("edge select editor", state.edgeSelection) {
            @Override
            public void proc(List<Edge> list) {
              if (list.size() <= 1) {
                Iterator<Tab> i = state.tabs.getTabs().iterator();
                while (i.hasNext()) {
                  Tab n = i.next();
                  Object u = n.getContent();
                  if (u == null) continue;
                  if (u.getClass() == EdgeEditor.class || u.getClass() == MultiEdgeEdit.class) {
                    if (u.getClass() == EdgeEditor.class) ((EdgeEditor) u).destroy();
                    if (u.getClass() == MultiEdgeEdit.class) ((MultiEdgeEdit) u).destroy();
                    i.remove();
                    break;
                  }
                }

                if (list.size() == 1) {
                  state.tabs.add("Edge", new EdgeEditor(state, list.get(0)));
                }
              } else if (list.size() > 1) {
                boolean created = false;
                Iterator<Tab> i = state.tabs.getTabs().iterator();
                while (i.hasNext()) {
                  Tab n = i.next();
                  Object u = n.getContent();
                  if (u == null) continue;
                  if (u.getClass() == EdgeEditor.class) {
                    ((EdgeEditor) u).destroy();
                    i.remove();
                    break;
                  } else if (u.getClass() == MultiEdgeEdit.class) {
                    created = true;
                  }
                }
                if (!created) {
                  state.tabs.add("Multi", new MultiEdgeEdit(state));
                }
              }
            }
          };

          // Populate data
          project.nodes.mirror(
              canvas.nodeLayer.getChildren(),
              n -> new NodeWidget(state, n).widget(),
              n -> ((NodeWidget) n.getUserData()).destroy(),
              (at, n) -> {});
          project.edges.mirror(
              canvas.edgeLayer.getChildren(),
              e -> new EdgeWidget(state, e).widget(),
              e -> ((EdgeWidget) e.getUserData()).destroy(),
              (at, n) -> {});

          if (Files.exists(absPath)) {
            loadGraph(model, Graph.load(absPath));
          } else {
            project.nodes.add(null, ImmutableList.of(createDefaultNode(model)));
          }
          state.graphLayout.mark();
          state.select(project.nodes.get(0));
        });
  }

  @SuppressWarnings("rawtypes")
  public static Optional confirmClose(Stage primaryStage) {
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    alert.initOwner(primaryStage);
    alert.initModality(Modality.WINDOW_MODAL);
    alert.setTitle("Unsaved changes");
    alert.setHeaderText("Your graph has unsaved changes.");
    alert.setContentText("If you close without saving you'll lose unsaved changes. Close anyway?");
    Optional<ButtonType> res = alert.showAndWait();
    return res.flatMap(b -> b == ButtonType.OK ? Optional.of(Main.class) : Optional.empty());
  }

  private void loadGraph(Model model, Graph graph) {
    class Inner {
      public void accept(Model m) {
        Project project = (Project) m.root;
        project.style.set(null, graph.style);
        Map<String, Node> nodeLookup = new HashMap<>();
        project.nodes.add(
            null,
            graph.nodes.stream()
                .map(n -> recurseBuildNode(m, nodeLookup, n))
                .collect(Collectors.toList()));
        project.edges.add(
            null,
            graph.edges.stream()
                .map(
                    edge -> {
                      Edge edge1 = Edge.create(m);
                      edge1.color.set(null, edge.contents.getOrDefault("color", ""));
                      edge1.from.set(null, nodeLookup.get(edge.from));
                      edge1.to.set(null, nodeLookup.get(edge.to));
                      return edge1;
                    })
                .collect(Collectors.toList()));
      }

      private Node recurseBuildNode(
          Model m, Map<String, Node> nodeLookup, com.zarbosoft.docgraph.core.Node node) {
        Node node1 = Node.create(m);
        nodeLookup.put(node.id, node1);
        node1.justCreated.set(false);
        node1.text.set(null, node.contents.getOrDefault("text", ""));
        node1.color.set(null, node.contents.getOrDefault("color", ""));
        node1.image.set(null, node.contents.getOrDefault("image", ""));
        node1.children.add(
            null,
            node.children.stream()
                .map(
                    n -> {
                      Node child = recurseBuildNode(m, nodeLookup, n);
                      child.parent.set(null, node1);
                      return child;
                    })
                .collect(Collectors.toList()));
        return node1;
      }
    }
    Inner inner = new Inner();
    inner.accept(model);
  }
}
