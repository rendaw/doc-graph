package com.zarbosoft.docgraph.gui.widget;

import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.docgraph.gui.State;
import com.zarbosoft.docgraph.gui.linkage.ScalarLinkDest;
import com.zarbosoft.docgraph.gui.model.latest.Edge;
import com.zarbosoft.docgraph.gui.model.latest.Segment;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

import static com.zarbosoft.docgraph.gui.Global.LINE_THICKNESS;
import static com.zarbosoft.docgraph.gui.Global.UNIT;

public class EdgeWidget {
  public static final double ARROW_SIZE = UNIT / 4;
  private final Link geomListener;
  private final Edge edge;
  private final Group group = new Group();
  private final Polygon arrow =
      new Polygon(0, 0, -ARROW_SIZE * 2, -ARROW_SIZE, -ARROW_SIZE * 2, ARROW_SIZE);
  private final State state;
  private final ScalarLinkDest<Boolean> selectedListener;
  private final EventHandler<MouseEvent> mouseListner;

  public EdgeWidget(State state, Edge edge) {
    this.edge = edge;
    group.setUserData(this);
    this.state = state;
    geomListener =
        new Link() {
          {
            mark();
          }

          @Override
          public boolean process() {
            group.getChildren().clear();
            for (int i = 0; i < EdgeWidget.this.edge.segments.size(); ++i) {
              Segment segment = EdgeWidget.this.edge.segments.get(i);
              Point2D start =
                  new Point2D(
                      segment.start.get().x.get().doubleValue(),
                      segment.start.get().y.get().doubleValue());
              Point2D end =
                  new Point2D(
                      segment.end.get().x.get().doubleValue(),
                      segment.end.get().y.get().doubleValue());
              double length = start.distance(end);
              Rectangle segment1 = new Rectangle(length + LINE_THICKNESS, LINE_THICKNESS);
              segment1.addEventFilter(MouseEvent.MOUSE_CLICKED, mouseListner);
              segment1.setFill(color());
              segment1.setArcWidth(LINE_THICKNESS);
              segment1.setArcHeight(LINE_THICKNESS);
              segment1.setLayoutX(start.getX());
              segment1.setLayoutY(start.getY());
              double angle =
                  Math.atan2(end.getY() - start.getY(), end.getX() - start.getX())
                      / (2.0 * Math.PI)
                      * 360;
              Translate arcTranslate = new Translate(-LINE_THICKNESS / 2, 0);
              segment1
                  .getTransforms()
                  .setAll(new Rotate(angle), arcTranslate, new Translate(0, -LINE_THICKNESS / 2));
              if (i == 0) {
                segment1.setArcWidth(0);
                segment1.setArcHeight(0);
                segment1.setWidth(segment1.getWidth() - LINE_THICKNESS);
                segment1.getTransforms().remove(arcTranslate);
              }
              if (i == EdgeWidget.this.edge.segments.size() - 1) {
                segment1.setWidth(segment1.getWidth() - ARROW_SIZE + 0.1);
                arrow.setFill(color());
                arrow.getTransforms().setAll(new Rotate(angle));
                arrow.setLayoutX(end.getX());
                arrow.setLayoutY(end.getY());
                group.getChildren().add(arrow);
              }
              group.getChildren().add(segment1);
            }
            return true;
          }

          @Override
          public String comment() {
            return "edge geom";
          }
        }.addSource(state.graphLayout);
    selectedListener =
        new ScalarLinkDest<>("edge widget selected", edge.selected) {
          @Override
          protected void proc(Boolean value) {
            for (Node child : group.getChildren()) {
              Shape shape = (Shape) child;
              shape.setFill(color());
            }
          }
        };
    mouseListner =
        e ->
            Link.wrap(
                () -> {
                  if (e.getButton() == MouseButton.PRIMARY) {
                    e.consume();
                    if (e.isControlDown() || e.isShiftDown()) {
                      state.selectEdgeToggle(edge);
                    } else {
                      state.select(edge);
                    }
                  }
                });
    this.arrow.addEventFilter(MouseEvent.MOUSE_CLICKED, mouseListner);
  }

  private Color color() {
    if (edge.selected.get()) return Color.BLUEVIOLET;
    try {
      return Color.valueOf(edge.color.get());
    } catch (Exception e) {
      return Color.BLACK;
    }
  }

  public Node widget() {
    return group;
  }

  public void destroy() {
    geomListener.removeSource(state.graphLayout);
    selectedListener.destroy();
  }
}
