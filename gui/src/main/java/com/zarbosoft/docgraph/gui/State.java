package com.zarbosoft.docgraph.gui;

import com.google.common.collect.ImmutableList;
import com.zarbosoft.automodel.lib.ChangeStep;
import com.zarbosoft.automodel.lib.History;
import com.zarbosoft.automodel.lib.link.ListLink;
import com.zarbosoft.automodel.lib.link.ListLinkBase;
import com.zarbosoft.automodel.lib.link.ScalarLink;
import com.zarbosoft.docgraph.core.Graph;
import com.zarbosoft.docgraph.core.render.Render;
import com.zarbosoft.docgraph.gui.editor.EdgeEditor;
import com.zarbosoft.docgraph.gui.editor.MultiEdgeEdit;
import com.zarbosoft.docgraph.gui.editor.MultiNodeEdit;
import com.zarbosoft.docgraph.gui.editor.NodeEditor;
import com.zarbosoft.docgraph.gui.model.latest.Edge;
import com.zarbosoft.docgraph.gui.model.latest.GraphDirection;
import com.zarbosoft.docgraph.gui.model.latest.Model;
import com.zarbosoft.docgraph.gui.model.latest.Node;
import com.zarbosoft.docgraph.gui.model.latest.Point;
import com.zarbosoft.docgraph.gui.model.latest.Project;
import com.zarbosoft.docgraph.gui.model.latest.Segment;
import com.zarbosoft.docgraph.gui.model.latest.SelectState;
import com.zarbosoft.docgraph.gui.widget.Focuser;
import com.zarbosoft.docgraph.gui.widget.Tabs;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.Common;
import com.zarbosoft.rendaw.common.Pair;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.control.Tab;
import org.decimal4j.immutable.Decimal4f;
import org.eclipse.elk.alg.layered.options.LayeredOptions;
import org.eclipse.elk.core.RecursiveGraphLayoutEngine;
import org.eclipse.elk.core.math.ElkPadding;
import org.eclipse.elk.core.options.CoreOptions;
import org.eclipse.elk.core.options.Direction;
import org.eclipse.elk.core.options.HierarchyHandling;
import org.eclipse.elk.core.options.NodeLabelPlacement;
import org.eclipse.elk.core.util.BasicProgressMonitor;
import org.eclipse.elk.graph.ElkBendPoint;
import org.eclipse.elk.graph.ElkEdge;
import org.eclipse.elk.graph.ElkEdgeSection;
import org.eclipse.elk.graph.ElkLabel;
import org.eclipse.elk.graph.ElkNode;
import org.eclipse.elk.graph.properties.IProperty;
import org.eclipse.elk.graph.properties.Property;
import org.eclipse.elk.graph.util.ElkGraphUtil;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.zarbosoft.docgraph.core.Global.jackson;
import static com.zarbosoft.docgraph.gui.model.latest.GraphDirection.DOWN;
import static com.zarbosoft.docgraph.gui.model.latest.GraphDirection.RIGHT;
import static com.zarbosoft.rendaw.common.Common.last;
import static com.zarbosoft.rendaw.common.Common.lastOpt;
import static com.zarbosoft.rendaw.common.Common.sublist;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public class State {
  public final Path path;
  public final Model model;
  public final Project root;
  public final ScalarLink<Boolean> drawerExpanded =
      new ScalarLink<>(false) {
        @Override
        public String comment() {
          return "drawer expanded";
        }
      };
  public final ListLink<Node> nodeSelection =
      new ListLink<>(new ArrayList<>()) {
        @Override
        public String comment() {
          return "node selection";
        }
      };
  public final ListLink<Edge> edgeSelection =
      new ListLink<>(new ArrayList<>()) {
        @Override
        public String comment() {
          return "edge selection";
        }
      };
  public final IProperty<Object> elkUserData = new Property<>("userdata");
  public final ScalarLink<GeomResult> graphLayout;
  public Tabs tabs;
  public ScalarLink<Optional<Node>> maxOneBefore =
      (ScalarLink<Optional<Node>>)
          new ScalarLink<>(null) {
            @Override
            public boolean process() {
              if (nodeSelection.size() != 1) this.value = null;
              else value = hasMaxOneBefore(nodeSelection.get(0));
              return true;
            }

            @Override
            public String comment() {
              return "max one before";
            }
          }.addSource(nodeSelection);
  public ScalarLink<Pair<Integer, Edge>> connected =
      (ScalarLink<Pair<Integer, Edge>>)
          new ScalarLink<>(null) {
            @Override
            public boolean process() {
              if (nodeSelection.size() != 2) this.value = null;
              else this.value = isConnected(nodeSelection.get(0), nodeSelection.get(1));
              return true;
            }

            @Override
            public String comment() {
              return "selection 2 connected";
            }
          }.addSource(nodeSelection);
  public ScalarLink<Point2D> scroll = new ScalarLink<>(Point2D.ZERO);
  public ScalarLink<Integer> zoom = new ScalarLink<>(0);
  public ScalarLink<Boolean> dirty = new ScalarLink<Boolean>(false);
  ChangeStep dirtyBase;

  public State(Path path, Model model, Project root) {
    this.path = path;
    this.model = model;
    this.root = root;
    dirtyBase = lastOpt(model.history.undoHistory).orElse(null);
    this.graphLayout =
        (ScalarLink<GeomResult>)
            new ScalarLink<GeomResult>(null) {
              public void generateNodes(ElkNode parent, Map<Node, ElkNode> nodeLookup, Node node) {
                ElkNode elkNode = ElkGraphUtil.createNode(parent);
                Render.zeroPaddings(elkNode);
                double pad = Global.defaultPad.doubleValue();
                elkNode.setProperty(LayeredOptions.PADDING, new ElkPadding(pad));
                elkNode.setProperty(LayeredOptions.SPACING_NODE_NODE, pad);
                elkNode.setProperty(LayeredOptions.SPACING_NODE_NODE_BETWEEN_LAYERS, pad);
                elkNode.setProperty(LayeredOptions.SPACING_EDGE_NODE, pad);
                elkNode.setProperty(LayeredOptions.SPACING_EDGE_NODE_BETWEEN_LAYERS, pad);
                elkNode.setProperty(LayeredOptions.SPACING_EDGE_EDGE, pad);
                elkNode.setProperty(LayeredOptions.SPACING_EDGE_EDGE_BETWEEN_LAYERS, pad);
                elkNode.setProperty(LayeredOptions.DIRECTION, directionToElk(getDirection(node)));
                double unitConversion = 3;
                double contentWidth = 20 * unitConversion;
                double contentHeight = 10;
                if (!node.image.get().isEmpty()) {
                  contentHeight += 10;
                  // if (!node.children.isEmpty()) contentHeight -= 10;
                }
                contentHeight *= unitConversion;
                elkNode.setWidth(Global.defaultPad.doubleValue() * 2 + contentWidth);
                elkNode.setHeight(Global.defaultPad.doubleValue() * 2 + contentHeight);

                if (!node.children.isEmpty()) {
                  elkNode.setProperty(LayeredOptions.SPACING_LABEL_NODE, Global.TEXT_PADDING);
                  elkNode.setHeight(pad * 2);
                  elkNode.setWidth(pad * 2);
                  ElkLabel label = ElkGraphUtil.createLabel(elkNode);
                  label.setHeight(contentHeight);
                  label.setWidth(contentWidth);
                  label.setProperty(
                      LayeredOptions.NODE_LABELS_PLACEMENT, NodeLabelPlacement.insideTopCenter());
                }
                elkNode.setProperty(elkUserData, node);
                nodeLookup.put(node, elkNode);
                for (Node child : node.children) {
                  generateNodes(elkNode, nodeLookup, child);
                }
              }

              @Override
              public boolean process() {
                ElkNode elkRoot = ElkGraphUtil.createGraph();
                Render.zeroPaddings(elkRoot);
                elkRoot.setProperty(CoreOptions.ALGORITHM, LayeredOptions.ALGORITHM_ID);
                double pad = Global.defaultPad.doubleValue();
                elkRoot.setProperty(LayeredOptions.SPACING_NODE_NODE, pad * 2);
                elkRoot.setProperty(LayeredOptions.SPACING_NODE_NODE_BETWEEN_LAYERS, pad * 2);
                elkRoot.setProperty(LayeredOptions.SPACING_EDGE_NODE, pad);
                elkRoot.setProperty(LayeredOptions.SPACING_EDGE_NODE_BETWEEN_LAYERS, pad);
                elkRoot.setProperty(LayeredOptions.SPACING_EDGE_EDGE, pad);
                elkRoot.setProperty(LayeredOptions.SPACING_EDGE_EDGE_BETWEEN_LAYERS, pad);
                elkRoot.setProperty(
                    LayeredOptions.HIERARCHY_HANDLING, HierarchyHandling.INCLUDE_CHILDREN);
                elkRoot.setProperty(LayeredOptions.DIRECTION, Direction.RIGHT);
                Map<Node, ElkNode> nodeLookup = new HashMap<>();
                for (Node node : root.nodes) {
                  generateNodes(elkRoot, nodeLookup, node);
                }

                List<ElkEdge> elkEdges = new ArrayList<>();
                for (Edge edge : root.edges) {
                  ElkNode sourceNode = nodeLookup.get(edge.from.get());
                  ElkNode destNode = nodeLookup.get(edge.to.get());
                  ElkNode parent;
                  if (sourceNode.getParent() == destNode.getParent())
                    parent = sourceNode.getParent();
                  else parent = elkRoot;
                  ElkEdge elkEdge = ElkGraphUtil.createEdge(parent);
                  /*
                  ElkPort sourcePort = null;
                  int edgeHashRes = edgeHash(edge);
                  String outKey = String.format("%s_out", edgeHashRes);
                  for (ElkPort port : sourceNode.getPorts()) {
                    if (!outKey.equals(port.getIdentifier())) continue;
                    sourcePort = port;
                    break;
                  }
                  if (sourcePort == null) {
                    sourcePort = ElkGraphUtil.createPort(sourceNode);
                    sourcePort.setIdentifier(outKey);
                  }
                  String inKey = String.format("%s_in", edgeHashRes);
                  ElkPort destPort = null;
                  for (ElkPort port : destNode.getPorts()) {
                    if (!inKey.equals(port.getIdentifier())) continue;
                    destPort = port;
                    break;
                  }
                  if (destPort == null) {
                    destPort = ElkGraphUtil.createPort(destNode);
                    destPort.setIdentifier(inKey);
                  }
                  elkEdge.getSources().add(sourcePort);
                  elkEdge.getTargets().add(destPort);
                   */
                  elkEdge.getSources().add(sourceNode);
                  elkEdge.getTargets().add(destNode);
                  elkEdge.setProperty(elkUserData, edge);
                  elkEdges.add(elkEdge);
                }
                /*
                System.out.println("");
                System.out.println("BEFORE");
                System.out.println(ElkGraphJson.forGraph(elkRoot).prettyPrint(true).toJson());
                 */
                RecursiveGraphLayoutEngine elk = new RecursiveGraphLayoutEngine();
                elk.layout(elkRoot, new BasicProgressMonitor());
                /*
                System.out.println("AFTER");
                System.out.println(ElkGraphJson.forGraph(elkRoot).prettyPrint(true).toJson());
                 */

                Point2D center = new Point2D(0, 0);

                for (ElkNode child : elkRoot.getChildren()) {
                  syncNodes(Point2D.ZERO, child);
                  center =
                      center.add(
                          new Point2D(
                              child.getX() + child.getWidth() / 2,
                              child.getY() + child.getHeight() / 2));
                }

                center = center.multiply(1.0 / elkRoot.getChildren().size());

                for (ElkEdge elkEdge : elkEdges) {
                  Edge edge = (Edge) elkEdge.getProperty(elkUserData);
                  edge.segments.clear();
                  ElkEdgeSection section = elkEdge.getSections().get(0);
                  Point2D basis = Point2D.ZERO;
                  ElkNode at = elkEdge.getContainingNode();
                  while (at != null) {
                    basis = basis.add(new Point2D(at.getX(), at.getY()));
                    at = at.getParent();
                  }
                  Point2D last = basis.add(new Point2D(section.getStartX(), section.getStartY()));
                  for (ElkBendPoint bendPoint : section.getBendPoints()) {
                    Point2D next = basis.add(new Point2D(bendPoint.getX(), bendPoint.getY()));
                    edge.segments.add(createSegment(last, next));
                    last = next;
                  }
                  edge.segments.add(
                      createSegment(
                          last, basis.add(new Point2D(section.getEndX(), section.getEndY()))));
                }
                this.value = new GeomResult(center);
                return true;
              }

              @Override
              public String comment() {
                return "graph layout";
              }

              public Direction directionToElk(GraphDirection source) {
                switch (source) {
                  case UP:
                    return Direction.UP;
                  case DOWN:
                    return Direction.DOWN;
                  case LEFT:
                    return Direction.LEFT;
                  case RIGHT:
                    return Direction.RIGHT;
                  default:
                    throw new IllegalArgumentException();
                }
              }

              private Segment createSegment(Point2D start, Point2D end) {
                Segment segment = Segment.create(model);
                Point start1 = Point.create(model);
                start1.x.set(Decimal4f.valueOf(start.getX()));
                start1.y.set(Decimal4f.valueOf(start.getY()));
                Point end1 = Point.create(model);
                end1.x.set(Decimal4f.valueOf(end.getX()));
                end1.y.set(Decimal4f.valueOf(end.getY()));
                segment.start.set(start1);
                segment.end.set(end1);
                return segment;
              }

              private void syncNodes(Point2D offset, ElkNode elkNode) {
                Point2D newOffset = offset.add(elkNode.getX(), elkNode.getY());
                Node node = (Node) elkNode.getProperty(elkUserData);
                node.x.set(Decimal4f.valueOf(newOffset.getX()));
                node.y.set(Decimal4f.valueOf(newOffset.getY()));
                node.width.set(Decimal4f.valueOf(elkNode.getWidth()));
                node.height.set(Decimal4f.valueOf(elkNode.getHeight()));
                for (ElkNode child : elkNode.getChildren()) {
                  syncNodes(newOffset, child);
                }
              }
            };
  }

  /*
  private int edgeHash(Edge edge) {
    return edge.contents.stream()
        .map(p -> new Pair<>(p.key.get(), p.value.get()))
        .filter(p -> !(p.first.isEmpty() && p.second.isEmpty()))
        .sorted()
        .collect(Collectors.toList())
        .hashCode();
  }
   */

  public GraphDirection getDirection(Node node) {
    Node at = node;
    int depth = 0;
    while (at != null) {
      at = at.parent.get();
      depth += 1;
    }
    return depth % 2 == 0 ? RIGHT : DOWN;
  }

  public static Node createDefaultNode(Model model) {
    Node node = createNodeBase(model);
    node.x.set(Global.defaultWidth.divide(2).negate());
    node.y.set(Global.defaultHeight.divide(2).negate());
    node.width.set(Global.defaultWidth);
    node.height.set(Global.defaultHeight);
    return node;
  }

  public void toggleInGroup(List<Node> nodes, Node other) {
    List<Node> notInOther = new ArrayList<>();
    List<Node> inOther = new ArrayList<>();
    for (Node node : nodes) {
      if (node == other) continue;
      if (node.parent.get() != other) {
        notInOther.add(node);
      } else {
        inOther.add(node);
      }
    }
    if (!notInOther.isEmpty()) {
      addToGroup(notInOther, other);
    } else {
      removeFromGroup(inOther, other);
    }
  }

  public void removeFromGroup(List<Node> nodes, Node other) {
    Node parentParent = other.parent.get();
    change(
        null,
        c -> {
          for (Node node : nodes) {
            if (node == other) continue;
            other.children.remove(c, other.children.indexOfTyped(node), 1);
            if (parentParent != null) {
              parentParent.children.add(c, node);
              node.parent.set(c, parentParent);
            } else {
              root.nodes.add(c, node);
              node.parent.set(c, null);
            }
          }
        });
  }

  public void addToGroup(List<Node> nodes, Node other) {
    change(
        null,
        c -> {
          for (Node node : nodes) {
            if (node == other) continue;
            Node parent = node.parent.get();
            if (parent == null) {
              root.nodes.remove(c, root.nodes.indexOfTyped(node), 1);
            } else {
              parent.children.remove(c, parent.children.indexOfTyped(node), 1);
            }
            other.children.add(c, node);
            node.parent.set(c, other);
          }
        });
  }

  public void selectNodeToggle(Node node) {
    deselectEdges();
    int index = nodeSelection.indexOfTyped(node);
    if (index >= 0) {
      ((List<Node>) nodeSelection).get(index).selected.set(SelectState.NONE);
      nodeSelection.remove(index, 1);
    } else {
      node.selected.set(
          ((List<Node>) nodeSelection).isEmpty() ? SelectState.PRIMARY : SelectState.SECONDARY);
      nodeSelection.add(node);
    }
  }

  public void deselectEdges() {
    for (Edge edge1 : edgeSelection) {
      edge1.selected.set(false);
    }
    edgeSelection.clear();
  }

  public void selectEdgeToggle(Edge edge) {
    deselectNodes();
    int index = edgeSelection.indexOfTyped(edge);
    if (index >= 0) {
      ((List<Edge>) edgeSelection).get(index).selected.set(false);
      edgeSelection.remove(index, 1);
    } else {
      edge.selected.set(true);
      edgeSelection.add(edge);
    }
  }

  public void deselectNodes() {
    for (Node node1 : nodeSelection) {
      node1.selected.set(SelectState.NONE);
    }
    nodeSelection.clear();
  }

  public void selectAfter(Node node) {
    for (Edge edge : root.edges) {
      if (edge.from.get() == node && edge.to.get() != node) {
        select(edge.to.get());
        return;
      }
    }
    if (node.parent.get() != null) {
      select(node.parent.get());
    }
  }

  public void select(Node node) {
    if (nodeSelection.size() == 1 && nodeSelection.get(0) == node) return;
    deselect();
    nodeSelection.add(node);
    node.selected.set(SelectState.PRIMARY);
    if (node.justCreated.get()) {
      editNodes(ImmutableList.of(node));
    }
  }

  public void deselect() {
    deselectEdges();
    deselectNodes();
  }

  public void editNodes(List<Node> nodes) {
    if (drawerExpanded.get()) {
      for (Tab tab : tabs.getTabs()) {
        javafx.scene.Node c = tab.getContent();
        if ((nodes.size() == 1 && c.getClass() != NodeEditor.class)
            || (nodes.size() != 1 && c.getClass() != MultiNodeEdit.class)) continue;
        if (tabs.getSelectionModel().getSelectedItem() == tab) {
          // nop
        } else {
          tabs.getSelectionModel().clearSelection();
          tabs.getSelectionModel().select(tab);
        }
        break;
      }
      delay(
          () -> {
            for (Tab tab : tabs.getTabs()) {
              javafx.scene.Node c = tab.getContent();
              if ((nodes.size() == 1 && c.getClass() != NodeEditor.class)
                  || (nodes.size() != 1 && c.getClass() != MultiNodeEdit.class)) continue;
              ((Focuser) c).focus();
              break;
            }
          });
    } else drawerExpanded.set(true);
  }

  public static void delay(Runnable x) {
    // This is the dumbest thing ever
    // Why doesn't javafx have a decent way to request focus
    Timer tim = new Timer();
    tim.schedule(
        new TimerTask() {
          @Override
          public void run() {
            Platform.runLater(x);
            tim.cancel();
          }
        },
        50);
  }

  public void selectBefore(Node node) {
    Node found = findBefore(node);
    if (found == null) return;
    select(found);
  }

  public Node findBefore(Node node) {
    for (Edge edge : root.edges) {
      if (edge.to.get() == node && edge.from.get() != node) {
        return edge.from.get();
      }
    }
    if (node.parent.get() != null) {
      return node.parent.get();
    }
    return null;
  }

  public void select(Edge edge) {
    if (edgeSelection.size() == 1 && edgeSelection.get(0) == edge) return;
    deselect();
    edgeSelection.add(edge);
    edge.selected.set(true);
  }

  public GraphDirection getParentDirection(Node node) {
    Node at = node;
    int depth = -1;
    while (at != null) {
      at = at.parent.get();
      depth += 1;
    }
    return depth % 2 == 0 ? RIGHT : DOWN;
  }

  public void undo() {
    model.history.undo();
    graphLayout.mark();
    updateDirty();
  }

  private void updateDirty() {
    dirty.set(
        !(model.history.changeStep.isEmpty()
            && lastOpt(model.history.undoHistory).orElse(null) == dirtyBase));
  }

  public void redo() {
    model.history.redo();
    graphLayout.mark();
    updateDirty();
  }

  public void connect(Node node1, Node node2) {
    // Must not be connected already (inv)
    change(
        null,
        c -> {
          Edge value = createEdgeBase(model);
          value.from.set(null, node1);
          value.to.set(null, node2);
          root.edges.add(c, value);
        });
    graphLayout.mark();
  }

  public void change(History.Tuple unique, Consumer<ChangeStep> consumer) {
    model.history.change(unique, consumer);
    updateDirty();
  }

  private static Edge createEdgeBase(Model model) {
    Edge edge = Edge.create(model);
    edge.color.set(null, "");
    return edge;
  }

  public Pair<Integer, Edge> isConnected(Node node1, Node node2) {
    // brute, todo optimize
    for (int i = 0; i < root.edges.size(); ++i) {
      Edge edge = root.edges.get(i);
      if (edge.from.get() == node1 && edge.to.get() == node2) {
        return new Pair<>(i, edge);
      }
    }
    return null;
  }

  public void disconnect(Pair<Integer, Edge> edge) {
    change(null, c -> root.edges.remove(c, edge.first, 1));
    graphLayout.mark();
  }

  public void delete(List<Node> nodes0) {
    if (nodes0.isEmpty()) throw new Assertion();
    Node before = findBefore(nodes0.get(0));
    Common.Mutable<Node> newNode = new Common.Mutable<>();
    change(
        null,
        c -> {
          Set<Node> nodes1 = new HashSet<>(nodes0);
          for (Node node : nodes0) {
            collectNodes(nodes1, node);
          }
          deleteNodesEdgesInner(c, nodes1);
          deleteNodesInner(c, nodes1);

          if (root.nodes.size() == 0) {
            newNode.value = createDefaultNode(model);
            root.nodes.add(c, newNode.value);
          }
        });
    graphLayout.mark();
    if (newNode.value == null) {
      if (before != null) select(before);
      else deselect();
    } else select(newNode.value);
  }

  private void collectNodes(Set<Node> nodesSet, Node node) {
    nodesSet.add(node);
    for (Node child : node.children) {
      collectNodes(nodesSet, child);
    }
  }

  public com.zarbosoft.docgraph.core.Node saveConvertNode(
      Map<Node, com.zarbosoft.docgraph.core.Node> nodeMap, Node node) {
    com.zarbosoft.docgraph.core.Node node1 = new com.zarbosoft.docgraph.core.Node();
    node1.id = "n" + nodeMap.size();
    nodeMap.put(node, node1);
    node1.contents = new HashMap<>();
    if (!node.text.get().isEmpty()) node1.contents.put("text", node.text.get());
    if (!node.color.get().isEmpty()) node1.contents.put("color", node.color.get());
    if (!node.image.get().isEmpty()) node1.contents.put("image", node.image.get());
    node1.children =
        node.children.stream().map(c -> saveConvertNode(nodeMap, c)).collect(Collectors.toList());
    return node1;
  }

  public void createBefore(Node node) {
    Common.Mutable<Node> ref = new Common.Mutable<>();
    change(
        null,
        c -> {
          Node node1 = createNodeBase(model);
          ref.value = node1;
          node1.width.set(Global.defaultWidth);
          node1.height.set(Global.defaultHeight);
          node1.x.set(node.x.get().subtract(Global.defaultWidth.add(Global.defaultPad)));
          node1.y.set(node.y.get());
          if (node.parent.get() == null) root.nodes.add(c, node1);
          else {
            node.parent.get().children.add(c, node1);
            node1.parent.set(c, node.parent.get());
          }

          Edge edge = createEdgeBase(model);
          edge.from.set(null, node1);
          edge.to.set(null, node);
          root.edges.add(c, edge);
        });
    select(ref.value);
    graphLayout.mark();
  }

  private static Node createNodeBase(Model model) {
    Node node = Node.create(model);
    node.text.set(null, "");
    node.color.set(null, "");
    node.image.set(null, "");
    return node;
  }

  public void editEdges(ListLink<Edge> edges) {
    if (drawerExpanded.get()) {
      for (Tab tab : tabs.getTabs()) {
        javafx.scene.Node c = tab.getContent();
        if ((edges.size() == 1 && c.getClass() != EdgeEditor.class)
            || (edges.size() != 1 && c.getClass() != MultiEdgeEdit.class)) continue;
        if (tabs.getSelectionModel().getSelectedItem() == tab) {
          // nop
        } else {
          tabs.getSelectionModel().clearSelection();
          tabs.getSelectionModel().select(tab);
        }
        break;
      }
      delay(
          () -> {
            for (Tab tab : tabs.getTabs()) {
              javafx.scene.Node c = tab.getContent();
              if ((edges.size() == 1 && c.getClass() != EdgeEditor.class)
                  || (edges.size() != 1 && c.getClass() != MultiEdgeEdit.class)) continue;
              ((Focuser) c).focus();
              break;
            }
          });
    } else drawerExpanded.set(true);
  }

  public void createAfter(Node node) {
    Common.Mutable<Node> ref = new Common.Mutable<>();
    change(
        null,
        c -> {
          Node node1 = createNodeBase(model);
          ref.value = node1;
          node1.width.set(Global.defaultWidth);
          node1.height.set(Global.defaultHeight);
          node1.x.set(node.x.get().add(Global.defaultPad));
          node1.y.set(node.y.get());
          if (node.parent.get() == null) root.nodes.add(c, node1);
          else {
            node.parent.get().children.add(c, node1);
            node1.parent.set(c, node.parent.get());
          }

          Edge edge = createEdgeBase(model);
          edge.from.set(null, node);
          edge.to.set(null, node1);
          root.edges.add(c, edge);
        });
    select(ref.value);
    graphLayout.mark();
  }

  public Optional<Node> hasMaxOneBefore(Node node) {
    // brute, todo optimize
    Optional<Node> before = Optional.empty();
    for (int i = 0; i < root.edges.size(); ++i) {
      Edge edge = root.edges.get(i);
      if (edge.to.get() == node) {
        if (before.isPresent()) {
          return null;
        } else {
          before = Optional.of(edge.from.get());
        }
      }
    }
    return before;
  }

  public void createParallel(Optional<Node> before, Node node) {
    Common.Mutable<Node> ref = new Common.Mutable<>();
    change(
        null,
        c -> {
          Node node1 = createNodeBase(model);
          ref.value = node1;
          node1.width.set(Global.defaultWidth);
          node1.height.set(Global.defaultHeight);
          node1.x.set(node.x.get());
          node1.y.set(node.y.get().add(Global.defaultPad.add(Global.defaultHeight)));
          if (node.parent.get() == null) {
            root.nodes.add(c, node1);
          } else {
            node.parent.get().children.add(c, node1);
            node1.parent.set(c, node.parent.get());
          }

          if (before.isPresent()) {
            Edge edge = createEdgeBase(model);
            edge.from.set(null, before.get());
            edge.to.set(null, node1);
            root.edges.add(c, edge);
          }
        });
    select(ref.value);
    graphLayout.mark();
  }

  public void create(Node within) {
    Common.Mutable<Node> ref = new Common.Mutable<>();
    change(
        null,
        c -> {
          Node node1 = createNodeBase(model);
          ref.value = node1;
          node1.width.set(Global.defaultWidth);
          node1.height.set(Global.defaultHeight);
          if (within == null) {
            root.nodes.add(c, node1);
          } else if (within != null) {
            node1.x.set(within.x.get().add(Global.defaultPad));
            node1.y.set(within.y.get());
            within.children.add(c, node1);
            node1.parent.set(c, within);
          }
        });
    select(ref.value);
    graphLayout.mark();
  }

  public void save() {
    model.history.finishChange();
    Common.atomicWrite(
        path,
        s -> {
          Graph out = new Graph();
          out.style = root.style.get();
          Map<Node, com.zarbosoft.docgraph.core.Node> nodeMap = new HashMap<>();
          out.nodes = new ArrayList<>();
          for (Node node : root.nodes) {
            out.nodes.add(saveConvertNode(nodeMap, node));
          }
          for (Edge edge : root.edges) {
            com.zarbosoft.docgraph.core.Edge edge1 = new com.zarbosoft.docgraph.core.Edge();
            edge1.id = "e" + out.edges.size();
            edge1.from = nodeMap.get(edge.from.get()).id;
            edge1.to = nodeMap.get(edge.to.get()).id;
            edge1.contents = new HashMap<>();
            if (!edge.color.get().isEmpty()) edge1.contents.put("color", edge.color.get());
            out.edges.add(edge1);
          }
          uncheck(
              () -> {
                jackson.writerWithDefaultPrettyPrinter().writeValue(s, (Object) out);
              });
        });
    dirtyBase = lastOpt(model.history.undoHistory).orElse(null);
    updateDirty();
  }

  public void group(ListLink<Node> nodes) {
    final Node commonParent;
    {
      List<Node> parents = null;
      for (Node node : nodes) {
        List<Node> parents1 = new ArrayList<>();
        Node at = node.parent.get();
        while (at != null) {
          parents1.add(at);
          at = at.parent.get();
        }
        Collections.reverse(parents1);
        if (parents == null) {
          parents = parents1;
        } else {
          int matchUntil = 0;
          for (; matchUntil < Math.min(parents.size(), parents1.size()); ++matchUntil) {
            if (parents.get(matchUntil) != parents1.get(matchUntil)) break;
          }
          parents = sublist(parents, 0, matchUntil);
          if (parents.isEmpty()) break;
        }
      }
      if (!parents.isEmpty()) commonParent = last(parents);
      else commonParent = null;
    }
    change(
        null,
        c -> {
          deleteNodesInner(c, nodes);

          Node node = createNodeBase(model);
          node.children.add(null, nodes);
          if (commonParent == null) {
            root.nodes.add(c, node);
          } else {
            commonParent.children.add(c, node);
            node.parent.set(c, commonParent);
          }
        });
    graphLayout.mark();
  }

  public void deleteNodesInner(ChangeStep c, Collection<Node> nodes0) {
    Map<Optional<Node>, Set<Node>> byParent =
        nodes0.stream()
            .collect(
                Collectors.groupingBy(
                    node -> Optional.ofNullable(node.parent.get()), Collectors.toSet()));
    for (Map.Entry<Optional<Node>, Set<Node>> e : byParent.entrySet()) {
      Node parent = e.getKey().orElse(null);
      Set<Node> remove = e.getValue();
      ListLinkBase<Node> children = parent == null ? root.nodes : parent.children;
      for (int i = 0; i < children.size(); ) {
        Node node = children.get(i);
        if (remove.contains(node)) {
          if (parent == null) {
            root.nodes.remove(c, i, 1);
          } else {
            parent.children.remove(c, i, 1);
          }
          remove.remove(node);
        } else {
          ++i;
        }
      }
    }
  }

  public void ungroup(Node node) {
    Node parent = node.parent.get();
    change(
        null,
        c -> {
          deleteNodesEdgesInner(c, ImmutableList.of(node));
          deleteNodesInner(c, ImmutableList.of(node));
          if (parent == null) {
            root.nodes.add(c, node.children);
          } else {
            parent.children.add(c, node.children);
          }
        });
    graphLayout.mark();
  }

  public void deleteNodesEdgesInner(ChangeStep c, Collection<Node> nodes0) {
    Set<Node> remove = new HashSet<>(nodes0);
    for (int i = 0; i < root.edges.size(); ) {
      Edge edge = root.edges.get(i);
      if (remove.contains(edge.from.get()) || remove.contains(edge.to.get())) {
        root.edges.remove(c, i, 1);
      } else {
        ++i;
      }
    }
  }

  public static class GeomResult {
    public final Point2D center;

    public GeomResult(Point2D center) {
      this.center = center;
    }
  }
}
