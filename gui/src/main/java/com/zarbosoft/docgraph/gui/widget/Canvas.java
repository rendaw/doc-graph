package com.zarbosoft.docgraph.gui.widget;

import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.docgraph.gui.State;
import com.zarbosoft.docgraph.gui.linkage.ScalarLinkDest;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import static com.zarbosoft.docgraph.gui.Global.defaultWidth;

public class Canvas {
  private final Pane canvasClip = new Pane();
  public final Pane layers = new Pane();
  public final Group nodeLayer = new Group();
  public final Group edgeLayer = new Group();
  public Point2D center = Point2D.ZERO;

  public Node widget() {
    return canvasClip;
  }

  public Canvas(State state) {
    layers.getChildren().add(nodeLayer);
    layers.getChildren().add(edgeLayer);
    canvasClip.setBackground(
        new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
    Rectangle clip = new Rectangle();
    clip.widthProperty().bind(canvasClip.widthProperty());
    clip.heightProperty().bind(canvasClip.heightProperty());
    canvasClip.getChildren().add(layers);
    canvasClip.setFocusTraversable(true);
    canvasClip.addEventHandler(
        MouseEvent.MOUSE_MOVED,
        e -> {
          canvasClip.requestFocus();
        });
    canvasClip.setMinWidth(defaultWidth.doubleValue() * 3);
    canvasClip.setMinHeight(defaultWidth.doubleValue() * 3);
    canvasClip.setMaxWidth(Double.MAX_VALUE);
    canvasClip.setMaxHeight(Double.MAX_VALUE);
    WidgetHelper.drag(
        canvasClip,
        MouseButton.PRIMARY,
        () -> state.scroll.get(),
        p -> {
          state.scroll.set(p.first.subtract(p.second));
        });
    class ScrollEventState {
      boolean longScroll;
      int startZoom;
    }
    ScrollEventState scrollEventState = new ScrollEventState();
    canvasClip.addEventFilter(
        ScrollEvent.SCROLL_STARTED,
        e -> {
          scrollEventState.longScroll = true;
          scrollEventState.startZoom = state.zoom.get();
        });
    canvasClip.addEventFilter(
        ScrollEvent.SCROLL_FINISHED,
        e1 -> {
          scrollEventState.longScroll = false;
        });
    canvasClip.addEventFilter(
        ScrollEvent.SCROLL,
        e1 -> Link.wrap(() -> {
          if (!scrollEventState.longScroll) {
            state.zoom.set(
                state.zoom.get()
                    + (int)
                        (e1.getTextDeltaYUnits() == ScrollEvent.VerticalTextScrollUnits.NONE
                            ? e1.getDeltaY() / 40
                            : e1.getTextDeltaY()));
          } else {
            state.zoom.set(scrollEventState.startZoom + (int) (e1.getTotalDeltaY() / 40));
          }
        }));
    canvasClip
        .layoutBoundsProperty()
        .addListener(
            ((observable, oldValue, newValue) ->
                Link.wrap(
                    () -> {
                      updateScroll(state);
                    })));
    canvasClip.setClip(clip);
    new ScalarLinkDest<Point2D>("scroll", state.scroll) {
      @Override
      public void proc(Point2D value) {
        updateScroll(state);
      }
    };
    new ScalarLinkDest<Integer>("zoom", state.zoom) {
      @Override
      protected void proc(Integer value) {
        double scale = Math.pow(1.5, value);
        layers.setScaleX(scale);
        layers.setScaleY(scale);
      }
    };
  }

  public void setCenter(State state, Point2D center) {
    state.scroll.inject(state.scroll.get().add(center.subtract(this.center)));
    this.center = center;
    updateScroll(state);
  }

  public void updateScroll(State state) {
    layers.setLayoutX(canvasClip.getWidth() / 2 - state.scroll.get().getX());
    layers.setLayoutY(canvasClip.getHeight() / 2 - state.scroll.get().getY());
  }

  public void addKeyHandler(EventHandler<KeyEvent> canvasKeyHandler) {
    canvasClip.addEventFilter(KeyEvent.KEY_PRESSED, canvasKeyHandler);
  }
}
