package com.zarbosoft.docgraph.gui.editor;

import com.zarbosoft.automodel.lib.History;
import com.zarbosoft.automodel.lib.link.Link;
import com.zarbosoft.docgraph.gui.State;
import com.zarbosoft.docgraph.gui.linkage.LinkJFXProxy;
import com.zarbosoft.docgraph.gui.linkage.ScalarLinkDest;
import com.zarbosoft.docgraph.gui.model.latest.Edge;
import com.zarbosoft.docgraph.gui.widget.Focuser;
import com.zarbosoft.docgraph.gui.widget.Grid;
import com.zarbosoft.docgraph.gui.widget.WidgetHelper;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class EdgeEditor extends ScrollPane implements Focuser {
  private final VBox box = new WidgetHelper.VBoxPad1();
  private final TextField colorField;
  private final LinkJFXProxy<String> colorProxy;
  private final ScalarLinkDest<String> colorProxy2;

  public EdgeEditor(State state, Edge edge) {
    setContent(box);
    setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
    setFitToWidth(true);

    // Content
    colorField = new TextField();
    colorProxy =
        new LinkJFXProxy<>(
            state, new History.Tuple(edge, "color"), colorField.textProperty(), edge.color);

    Grid grid = new Grid();
    ColorPicker p = new ColorPicker();
    p.setStyle("-fx-color-label-visible: false");
    grid.add("Border",colorField, p);
    p.setOnAction(
        e ->
            Link.wrap(
                () -> {
                  Color newValue = p.getValue();
                  state.change(
                      null,
                      c ->
                          edge.color.set(
                              c,
                              String.format(
                                  "rgb(%d, %d, %d)",
                                  (int) (newValue.getRed() * 256),
                                  (int) (newValue.getGreen() * 256),
                                  (int) (newValue.getBlue() * 256))));
                }));
    colorProxy2 =
        new ScalarLinkDest<>("reverse color", edge.color) {
          @Override
          protected void proc(String value) {
            Color c;
            try {
              c = Color.valueOf(edge.color.get());
            } catch (Exception e) {
              return;
            }
            p.setValue(c);
          }
        };

    box.getChildren().add(grid);
  }

  public void destroy() {
    colorProxy.destroy();
    colorProxy2.destroy();
  }

  @Override
  public void focus() {
    colorField.requestFocus();
  }
}
