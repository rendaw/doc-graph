module com.zarbosoft.docgraph.gui {
  requires com.zarbosoft.docgraph.core;
  requires com.zarobosoft.automodel.lib;
  requires decimal4j;
  requires org.eclipse.emf.common;
  requires javafx.graphics;
  requires javafx.controls;
  requires com.zarbosoft.rendaw.common;
  requires org.eclipse.elk.alg.layered;
  requires org.eclipse.elk.core;
  requires org.eclipse.elk.graph;
  requires com.google.common;
  opens com.zarbosoft.docgraph.gui;
  requires io.pebbletemplates;
  requires com.fasterxml.jackson.databind;
  // For scenicview...
  requires java.instrument;
  requires javafx.swing;
  requires java.rmi;
  // For debugging...
  requires jdk.jdwp.agent;
  requires jdk.management.agent;
}