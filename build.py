#!/usr/bin/env python3
import subprocess
import os
from pathlib import Path
import shutil
import argparse
import re

here = (Path(__file__).parent).resolve()

P_LINUX = "linux"
P_MAC = "mac"
P_WINDOWS = "windows"

parser = argparse.ArgumentParser()
parser.add_argument("host", choices=[P_LINUX, P_MAC, P_WINDOWS])
parser.add_argument("target", choices=[P_LINUX, P_MAC, P_WINDOWS])
args = parser.parse_args()

# More variables
if False:
    pass
elif args.host == P_LINUX:
    java_path = "/usr/lib/jvm/java-14-openjdk"
elif args.host == P_MAC:
    java_path = (
        "/Library/Java/JavaVirtualMachines/adoptopenjdk-14.jdk/Contents/Home"  # noqa
    )
else:
    raise AssertionError

# Debug
print("CWD", Path.cwd())

# Set up directories
build = here / "_build"
shutil.rmtree(build, ignore_errors=True)
os.makedirs(build, exist_ok=True)
if args.host != P_MAC:
    os.makedirs(Path.home() / ".m2", exist_ok=True)

# Tools
def c(*pargs, **kwargs):
    print(pargs, kwargs)
    env_source = kwargs.get("env")
    if env_source:
        env = os.environ.copy()
        env.update(env_source)
        kwargs["env"] = env
    subprocess.check_call(*pargs, **kwargs)


def mvn(*extra):
    c(
        [
            "mvn",
            *extra,
            "-B",
            "-Dstyle.color=never",
            f'-Dmaven.repo.local={Path.home() / ".m2" / "repository"}',
            "-e",
            "-global-toolchains",
            build / "toolchains.xml",
        ],
        env=dict(JAVA_HOME=java_path),
    )


def template(source, dest, extra):
    with open(source) as source_:
        text = source_.read()
    text = text.format(**extra)
    with open(dest, "w") as dest_:
        dest_.write(text)


# Build
template(
    "buildtemplate/toolchains.xml",
    build / "toolchains.xml",
    dict(java_home={
        P_LINUX: java_path,
        P_WINDOWS: "/java-14-windows",
        P_MAC: java_path,
    }[args.target]),
)

c(["mvn", "install"], cwd=here / "embed", env=dict(JAVA_HOME=java_path))
mvn(
    "clean",
    "package",
    "-DtargetPlatform={}".format(args.target),
    "-Djavafx.platform={}".format({
        P_LINUX: "linux",
        P_WINDOWS: "win",
        P_MAC: "mac",
    }[args.target]),
)
