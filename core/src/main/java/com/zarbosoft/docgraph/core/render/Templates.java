package com.zarbosoft.docgraph.core.render;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.error.PebbleException;
import com.mitchellbosecke.pebble.extension.AbstractExtension;
import com.mitchellbosecke.pebble.extension.Filter;
import com.mitchellbosecke.pebble.extension.Function;
import com.mitchellbosecke.pebble.extension.escaper.SafeString;
import com.mitchellbosecke.pebble.loader.FileLoader;
import com.mitchellbosecke.pebble.loader.Loader;
import com.mitchellbosecke.pebble.node.expression.UnaryExpression;
import com.mitchellbosecke.pebble.operator.UnaryOperator;
import com.mitchellbosecke.pebble.template.EvaluationContext;
import com.mitchellbosecke.pebble.template.EvaluationContextImpl;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import com.mitchellbosecke.pebble.template.PebbleTemplateImpl;
import com.zarbosoft.rendaw.common.Assertion;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.zarbosoft.rendaw.common.Common.substr;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public class Templates {
  static final String memKey = "mem:";
  static final String fsKey = "fs_:";

  public static PebbleEngine getPebble(Path basePath) {
    class InMemoryTemplateWrapper implements TemplateWrapper {

      private final String template;

      public InMemoryTemplateWrapper(String template) {
        this.template = template;
      }

      @Override
      public Reader reader() {
        return new StringReader(template);
      }

      @Override
      public String resolve(String relativePath) {
        return basePath.resolve(relativePath).normalize().toString();
      }

      @Override
      public boolean exists() {
        return true;
      }
    }
    FileLoader fileLoader = new FileLoader();
    class FSTemplateWrapper implements TemplateWrapper {

      private final String path;

      public FSTemplateWrapper(String path) {
        this.path = path;
      }

      @Override
      public Reader reader() {
        return fileLoader.getReader(path);
      }

      @Override
      public String resolve(String relativePath) {
        return fileLoader.resolveRelativePath(relativePath, path);
      }

      @Override
      public boolean exists() {
        return fileLoader.resourceExists(path);
      }
    }
    PebbleEngine pebble =
        new PebbleEngine.Builder()
            .loader(
                new Loader<String>() {
                  @Override
                  public Reader getReader(String cacheKey) {
                    return extract(cacheKey).reader();
                  }

                  @Override
                  public void setCharset(String charset) {
                    // unused (?)
                    throw new Assertion();
                  }

                  @Override
                  public void setPrefix(String prefix) {
                    // unused (?)
                    throw new Assertion();
                  }

                  @Override
                  public void setSuffix(String suffix) {
                    // unused (?)
                    throw new Assertion();
                  }

                  @Override
                  public String resolveRelativePath(String relativePath, String anchorPath) {
                    return fsKey + extract(anchorPath).resolve(relativePath);
                  }

                  @Override
                  public String createCacheKey(String templateName) {
                    return templateName;
                  }

                  @Override
                  public boolean resourceExists(String templateName) {
                    return extract(templateName).exists();
                  }

                  public TemplateWrapper extract(String name) {
                    String key = substr(name, 0, 4);
                    String val = substr(name, 4);
                    switch (key) {
                      case memKey:
                        return new InMemoryTemplateWrapper(val);
                      case fsKey:
                        return new FSTemplateWrapper(val);
                      default:
                        throw new Assertion();
                    }
                  }
                })
            .extension(
                new AbstractExtension() {
                  final Map<Object, Integer> uidLookup = new HashMap<>();

                  @Override
                  public List<UnaryOperator> getUnaryOperators() {
                    return ImmutableList.of(
                        new UnaryOperator() {

                          @Override
                          public int getPrecedence() {
                            return 500;
                          }

                          @Override
                          public String getSymbol() {
                            return "*";
                          }

                          @Override
                          public Class<? extends UnaryExpression> getNodeClass() {
                            return TruthyOperator.class;
                          }
                        });
                  }

                  @Override
                  public Map<String, Filter> getFilters() {
                    return ImmutableMap.<String, Filter>builder()
                        .put(
                            "default",
                            new Filter() {
                              @Override
                              public List<String> getArgumentNames() {
                                return ImmutableList.of("value");
                              }

                              @Override
                              public Object apply(
                                  Object input,
                                  Map<String, Object> args,
                                  PebbleTemplate self,
                                  EvaluationContext context,
                                  int lineNumber)
                                  throws PebbleException {
                                return truthy(input) ? input : args.get("value");
                              }
                            })
                        .build();
                  }

                  @Override
                  public Map<String, Function> getFunctions() {
                    return ImmutableMap.<String, Function>builder()
                        .put(
                            "rslice",
                            new Function() {
                              @Override
                              public Object execute(
                                  Map<String, Object> args,
                                  PebbleTemplate self,
                                  EvaluationContext context,
                                  int lineNumber) {
                                List list = (List) args.get("list");
                                long start = (long) args.get("start");
                                long end = (long) args.get("end");
                                if (start < 0) start = list.size() + start + 1;
                                if (end < 0) end = list.size() + end + 1;
                                return list.subList((int)start, (int)end);
                              }

                              @Override
                              public List<String> getArgumentNames() {
                                return ImmutableList.of("list", "start", "end");
                              }
                            })
                        .put(
                            "i",
                            new Function() {
                              @Override
                              public Object execute(
                                  Map<String, Object> args,
                                  PebbleTemplate self,
                                  EvaluationContext context,
                                  int lineNumber) {
                                long index = (long) args.get("index");
                                List list = (List) args.get("list");
                                if (index < 0) return list.get((int) (list.size() + index));
                                else return list.get((int) index);
                              }

                              @Override
                              public List<String> getArgumentNames() {
                                return ImmutableList.of("list", "index");
                              }
                            })
                        .put(
                            "vapproach",
                            new Function() {
                              @Override
                              public Object execute(
                                  Map<String, Object> args,
                                  PebbleTemplate self,
                                  EvaluationContext context,
                                  int lineNumber) {
                                Point from = (Point) args.get("from");
                                Point to = (Point) args.get("to");
                                Number distance = (Number) args.get("distance");
                                double dx = to.x - from.x;
                                double dy = to.y - from.y;
                                double l = Math.sqrt(dx * dx + dy * dy);
                                double ux = dx / l;
                                double uy = dy / l;
                                Point out = new Point();
                                out.x = from.x + ux * distance.doubleValue();
                                out.y = from.y + uy * distance.doubleValue();
                                return out;
                              }

                              @Override
                              public List<String> getArgumentNames() {
                                return ImmutableList.of("from", "to", "distance");
                              }
                            })
                        .put(
                            "uid",
                            new Function() {
                              int last = 0;

                              @Override
                              public Object execute(
                                  Map<String, Object> args,
                                  PebbleTemplate self,
                                  EvaluationContext context,
                                  int lineNumber) {
                                Object input = args.get("value");
                                return uidLookup.computeIfAbsent(
                                    input,
                                    k -> {
                                      return ++last;
                                    });
                              }

                              @Override
                              public List<String> getArgumentNames() {
                                return ImmutableList.of("value");
                              }
                            })
                        .put(
                            "uidIsNew",
                            new Function() {
                              @Override
                              public List<String> getArgumentNames() {
                                return ImmutableList.of("value");
                              }

                              @Override
                              public Object execute(
                                  Map<String, Object> args,
                                  PebbleTemplate self,
                                  EvaluationContext context,
                                  int lineNumber) {
                                return !uidLookup.containsKey(args.get("value"));
                              }
                            })
                        .put(
                            "embedSvg",
                            new Function() {
                              final Pattern widthHeightPattern =
                                  Pattern.compile(
                                      "<svg.*?width=\"(\\d+)\".*?height=\"(\\d+)\".*?>",
                                      Pattern.DOTALL);
                              final Pattern heightWidthPattern =
                                  Pattern.compile(
                                      "<svg.*?height=\"(\\d+)\".*?width=\"(\\d+)\".*?>",
                                      Pattern.DOTALL);

                              @Override
                              public List<String> getArgumentNames() {
                                return ImmutableList.of("path", "id");
                              }

                              @Override
                              public Object execute(
                                  Map<String, Object> args,
                                  PebbleTemplate self,
                                  EvaluationContext context,
                                  int lineNumber) {
                                String text =
                                    uncheck(
                                        () -> {
                                          try (InputStream is =
                                              Files.newInputStream(
                                                  Paths.get(
                                                      substr(
                                                          ((PebbleTemplateImpl) self)
                                                              .resolveRelativePath(
                                                                  (String) args.get("path")),
                                                          memKey.length())))) {
                                            return new String(
                                                is.readAllBytes(), StandardCharsets.UTF_8);
                                          }
                                        });
                                if (!text.contains("viewBox=")) {
                                  String width = null;
                                  String height = null;
                                  Matcher matcher = heightWidthPattern.matcher(text);
                                  if (matcher.find()) {
                                    width = matcher.group(2);
                                    height = matcher.group(1);
                                  } else {
                                    matcher = widthHeightPattern.matcher(text);
                                    if (matcher.find()) {
                                      width = matcher.group(1);
                                      height = matcher.group(2);
                                    }
                                  }
                                  if (width != null) {
                                    text =
                                        text.replaceAll(
                                            "<svg",
                                            String.format(
                                                "<svg viewBox=\"0 0 %s %s\"", width, height));
                                  }
                                }
                                text = text.replaceAll("<\\?xml .*?\\?>", "");
                                text =
                                    text.replaceAll(
                                        "<svg", String.format("<svg id=\"%s\"", args.get("id")));
                                text = text.trim();
                                return new SafeString(text + "\n");
                              }
                            })
                        .build();
                  }
                })
            .build();
    return pebble;
  }

  public static boolean truthy(Object arg) {
    if (arg == null) return false;
    else if (arg instanceof List) {
      return !(((List<?>) arg).isEmpty());
    } else if (arg instanceof Map) {
      return !(((Map<?, ?>) arg).isEmpty());
    } else if (arg instanceof String) {
      return !((String) arg).isEmpty();
    } else if (arg instanceof Integer) {
      return ((Integer) arg) == 0;
    } else if (arg instanceof Long) {
      return ((Long) arg) == 0;
    } else if (arg instanceof Float) {
      return ((Float) arg) == 0.0;
    } else if (arg instanceof Double) {
      return ((Double) arg) == 0.0;
    } else if (arg instanceof Boolean) {
      return (boolean) arg;
    } else throw new Assertion();
  }

  public static PebbleTemplate loadInMemory(PebbleEngine pebble, String template) {
    return pebble.getTemplate(memKey + template);
  }

  // Warning, nothing but hacks ahead
  private interface TemplateWrapper {

    Reader reader();

    String resolve(String relativePath);

    boolean exists();
  }

  public static class TruthyOperator extends UnaryExpression {
    @Override
    public Object evaluate(PebbleTemplateImpl self, EvaluationContextImpl context) {
      Object arg = getChildExpression().evaluate(self, context);
      return truthy(arg);
    }
  }
}
