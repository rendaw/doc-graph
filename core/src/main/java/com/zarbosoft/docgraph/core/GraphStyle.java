package com.zarbosoft.docgraph.core;

import com.esotericsoftware.yamlbeans.YamlReader;

import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public class GraphStyle {
  public double padding = 5;
  public String template;
  public String direction = "right";
  public String nodeContentWidth = "{{contents.width | default(20)}}";
  public String nodeContentHeight = "{{(contents.height | default(10)) + (*contents.image ? 10 : 0) - (contents.children ? 10 : 0)}}";
  public String nodeChildrenAlignment = "down";
  public String nodeDirection = "down";
  public String nodeTextPadding = "6";
  public String nodeTextHeight = "10";
  public String nodeHintColor = "{{contents.color}}";
  public String nodeHintText = "{{contents.text}}";
  public String nodeHintImage = "{{contents.image}}";

  public static GraphStyle load(Path graphPath, String stylePath) {
    return load(graphPath.toAbsolutePath().getParent().resolve(stylePath));
  }

  public static GraphStyle load(Path path) {
    if (Files.isRegularFile(path))
      return uncheck(() -> new YamlReader(new FileReader(path.toFile())).read(GraphStyle.class));
    return uncheck(
        () ->
            new YamlReader(new InputStreamReader(new ByteArrayInputStream(defaultstyle_yaml.data)))
                .read(GraphStyle.class));
  }

  public static GraphStyle loadExact(Path graphPath, String stylePath) {
    if (stylePath.isEmpty())
      return uncheck(
          () ->
              new YamlReader(
                      new InputStreamReader(new ByteArrayInputStream(defaultstyle_yaml.data)))
                  .read(GraphStyle.class));
    else
      return uncheck(
          () ->
              new YamlReader(
                      new FileReader(
                          graphPath.toAbsolutePath().getParent().resolve(stylePath).toFile()))
                  .read(GraphStyle.class));
  }
}
