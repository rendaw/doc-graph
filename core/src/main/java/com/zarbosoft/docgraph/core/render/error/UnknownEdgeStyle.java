package com.zarbosoft.docgraph.core.render.error;

public class UnknownEdgeStyle implements DefError {
  private final String style;

  public UnknownEdgeStyle(String style) {this.style = style;}

  @Override
  public String print() {
    return String.format("Unknown edge style [%s]", style);
  }
}
