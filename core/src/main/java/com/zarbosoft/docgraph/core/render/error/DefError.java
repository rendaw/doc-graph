package com.zarbosoft.docgraph.core.render.error;

public interface DefError {
  public String print();
}
