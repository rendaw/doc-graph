package com.zarbosoft.docgraph.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zarbosoft.appdirsj.AppDirs;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Global {
  public static final AppDirs appDirs = new AppDirs().set_appname("docgraph");
  public static final ObjectMapper jackson = new ObjectMapper();
}
