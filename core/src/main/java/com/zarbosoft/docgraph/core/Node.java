package com.zarbosoft.docgraph.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Node {
  public String id;
  public Map<String, String> contents;
  public String style;
  public List<Node> children = new ArrayList<>();
  public String direction;
}
