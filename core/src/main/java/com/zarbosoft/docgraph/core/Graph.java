package com.zarbosoft.docgraph.core;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.zarbosoft.docgraph.core.Global.jackson;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public class Graph {
  public String style;
  public List<Node> nodes;
  public List<Edge> edges = new ArrayList<>();

  public static Graph load(Path path) {
    return uncheck(() -> jackson.readValue(path.toFile(), Graph.class));
  }
}
