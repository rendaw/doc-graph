package com.zarbosoft.docgraph.core.render.error;

public class DuplicateNode implements DefError {
  private final String id;

  public DuplicateNode(String id) {this.id = id;}

  @Override
  public String print() {
    return String.format("Duplicate node id [%s]",id);
  }
}
