package com.zarbosoft.docgraph.core.render.error;

public class DuplicateEdge implements DefError {
  private final String id;

  public DuplicateEdge(String id) {this.id = id;}

  @Override
  public String print() {
    return String.format("Duplicate edge id [%s]",id);
  }
}
