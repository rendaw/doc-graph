package com.zarbosoft.docgraph.core.render.error;

public class EdgeBadEnd implements DefError {
  private final String end;
  private final String id;

  public EdgeBadEnd(String id, String end) {this.id = id; this.end = end;}

  @Override
  public String print() {
    return String.format("Unknown edge [%s] end node id [%s]", id, end);
  }
}
