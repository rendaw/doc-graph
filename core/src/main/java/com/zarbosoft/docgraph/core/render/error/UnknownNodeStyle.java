package com.zarbosoft.docgraph.core.render.error;

public class UnknownNodeStyle implements DefError {
  private final String style;

  public UnknownNodeStyle(String style) {
    this.style = style;
  }

  @Override
  public String print() {
    return String.format("Unknown node style [%s]", style);
  }
}
