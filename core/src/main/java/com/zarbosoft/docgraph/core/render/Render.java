package com.zarbosoft.docgraph.core.render;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import com.zarbosoft.docgraph.core.Edge;
import com.zarbosoft.docgraph.core.Graph;
import com.zarbosoft.docgraph.core.GraphStyle;
import com.zarbosoft.docgraph.core.Node;
import com.zarbosoft.docgraph.core.render.error.DefError;
import com.zarbosoft.docgraph.core.render.error.DuplicateEdge;
import com.zarbosoft.docgraph.core.render.error.DuplicateNode;
import com.zarbosoft.docgraph.core.render.error.EdgeBadEnd;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.Pair;
import org.eclipse.elk.alg.layered.options.LayeredOptions;
import org.eclipse.elk.core.RecursiveGraphLayoutEngine;
import org.eclipse.elk.core.math.ElkPadding;
import org.eclipse.elk.core.options.CoreOptions;
import org.eclipse.elk.core.options.Direction;
import org.eclipse.elk.core.options.HierarchyHandling;
import org.eclipse.elk.core.options.NodeLabelPlacement;
import org.eclipse.elk.core.util.BasicProgressMonitor;
import org.eclipse.elk.graph.ElkBendPoint;
import org.eclipse.elk.graph.ElkEdge;
import org.eclipse.elk.graph.ElkEdgeSection;
import org.eclipse.elk.graph.ElkLabel;
import org.eclipse.elk.graph.ElkNode;
import org.eclipse.elk.graph.util.ElkGraphUtil;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.zarbosoft.docgraph.core.Global.jackson;
import static com.zarbosoft.rendaw.common.Common.uncheck;
import static org.eclipse.elk.core.options.NodeLabelPlacement.H_LEFT;
import static org.eclipse.elk.core.options.NodeLabelPlacement.H_RIGHT;
import static org.eclipse.elk.core.options.NodeLabelPlacement.INSIDE;
import static org.eclipse.elk.core.options.NodeLabelPlacement.V_CENTER;

public class Render {
  public static void zeroPaddings(ElkNode n) {
    n.setProperty(LayeredOptions.PADDING, new ElkPadding(0));
    n.setProperty(LayeredOptions.SPACING_COMMENT_COMMENT, 0.0);
    n.setProperty(LayeredOptions.SPACING_COMMENT_NODE, 0.0);
    n.setProperty(LayeredOptions.SPACING_COMPONENT_COMPONENT, 0.0);
    n.setProperty(LayeredOptions.SPACING_EDGE_EDGE, 0.0);
    n.setProperty(LayeredOptions.SPACING_EDGE_LABEL, 0.0);
    n.setProperty(LayeredOptions.SPACING_EDGE_NODE, 0.0);
    n.setProperty(LayeredOptions.SPACING_LABEL_LABEL, 0.0);
    n.setProperty(LayeredOptions.SPACING_LABEL_PORT, 0.0);
    n.setProperty(LayeredOptions.SPACING_LABEL_NODE, 0.0);
    n.setProperty(LayeredOptions.SPACING_NODE_NODE, 0.0);
    n.setProperty(LayeredOptions.SPACING_NODE_SELF_LOOP, 0.0);
    n.setProperty(LayeredOptions.SPACING_PORT_PORT, 0.0);
    n.setProperty(LayeredOptions.SPACING_BASE_VALUE, 0.0);
    n.setProperty(LayeredOptions.SPACING_EDGE_EDGE_BETWEEN_LAYERS, 0.0);
    n.setProperty(LayeredOptions.SPACING_EDGE_NODE_BETWEEN_LAYERS, 0.0);
    n.setProperty(LayeredOptions.SPACING_NODE_NODE_BETWEEN_LAYERS, 0.0);
  }

  public static List<DefError> render(Path source, Path dest) {
    return uncheck(
        () -> {
          Graph graph = loadJson(source, Graph.class);

          GraphStyle style = GraphStyle.loadExact(source, graph.style);

          NodeStyleTemplates nodeStyleTemplates = new NodeStyleTemplates();
          PebbleEngine pebble = Templates.getPebble(source.getParent());
          nodeStyleTemplates.hintText = Templates.loadInMemory(pebble, style.nodeHintText);
          nodeStyleTemplates.width = Templates.loadInMemory(pebble, style.nodeContentWidth);
          nodeStyleTemplates.height = Templates.loadInMemory(pebble, style.nodeContentHeight);
          nodeStyleTemplates.direction = Templates.loadInMemory(pebble, style.nodeDirection);
          nodeStyleTemplates.childrenAlignment =
              Templates.loadInMemory(pebble, style.nodeChildrenAlignment);
          nodeStyleTemplates.textPadding = Templates.loadInMemory(pebble, style.nodeTextPadding);
          nodeStyleTemplates.textHeight = Templates.loadInMemory(pebble, style.nodeTextHeight);

          List<DefError> errors = new ArrayList<>();
          ElkNode elkRoot = ElkGraphUtil.createGraph();
          elkRoot.setProperty(CoreOptions.ALGORITHM, LayeredOptions.ALGORITHM_ID);
          zeroPaddings(elkRoot);
          elkRoot.setProperty(LayeredOptions.PADDING, new ElkPadding(style.padding));
          elkRoot.setProperty(LayeredOptions.SPACING_NODE_NODE, style.padding * 2);
          elkRoot.setProperty(LayeredOptions.SPACING_NODE_NODE_BETWEEN_LAYERS, style.padding * 2);
          elkRoot.setProperty(LayeredOptions.SPACING_EDGE_NODE, style.padding);
          elkRoot.setProperty(LayeredOptions.SPACING_EDGE_NODE_BETWEEN_LAYERS, style.padding);
          elkRoot.setProperty(LayeredOptions.SPACING_EDGE_EDGE, style.padding);
          elkRoot.setProperty(LayeredOptions.SPACING_EDGE_EDGE_BETWEEN_LAYERS, style.padding);
          elkRoot.setProperty(
              LayeredOptions.HIERARCHY_HANDLING, HierarchyHandling.INCLUDE_CHILDREN);
          elkRoot.setProperty(
              LayeredOptions.DIRECTION,
              parseDirection(style.direction == null ? "right" : style.direction));
          Map<String, ElkNode> nodeLookup = new HashMap<>();
          for (Node node : graph.nodes) {
            generateNodes(style, nodeStyleTemplates, elkRoot, errors, nodeLookup, node);
          }

          Map<String, ElkEdge> edgeLookup = new HashMap<>();
          for (Edge edge : graph.edges) {
            ElkEdge elkEdge = ElkGraphUtil.createEdge(elkRoot);
            ElkNode sourceNode = nodeLookup.get(edge.from);
            ElkNode destNode = nodeLookup.get(edge.to);
            if (sourceNode == null || destNode == null) {
              if (sourceNode == null) {
                errors.add(new EdgeBadEnd(edge.id, edge.from));
              }
              if (destNode == null) {
                errors.add(new EdgeBadEnd(edge.id, edge.to));
              }
              continue;
            }
            elkEdge.getSources().add(sourceNode);
            elkEdge.getTargets().add(destNode);

            if (edgeLookup.containsKey(edge.id)) {
              errors.add(new DuplicateEdge(edge.id));
            } else {
              edgeLookup.put(edge.id, elkEdge);
            }
          }

          if (!errors.isEmpty()) {
            return errors;
          }

          RecursiveGraphLayoutEngine elk = new RecursiveGraphLayoutEngine();
          elk.layout(elkRoot, new BasicProgressMonitor());

          double allLeft0 = Double.MAX_VALUE;
          double allTop0 = Double.MAX_VALUE;
          double allRight = -Double.MAX_VALUE;
          double allBottom = -Double.MAX_VALUE;
          for (Node node : graph.nodes) {
            ElkNode elkNode = nodeLookup.get(node.id);
            allLeft0 = Math.min(allLeft0, elkNode.getX());
            allTop0 = Math.min(allTop0, elkNode.getY());
            allRight = Math.max(allRight, elkNode.getX() + elkNode.getWidth());
            allBottom = Math.max(allBottom, elkNode.getY() + elkNode.getHeight());
          }
          allLeft0 -= style.padding;
          allTop0 -= style.padding;
          double allLeft = allLeft0;
          double allTop = allTop0;
          allRight += style.padding;
          allBottom += style.padding;
          double allWidth = allRight - allLeft;
          double allHeight = allBottom - allTop;

          List<Map<String, Object>> nodeData = new ArrayList<>();
          for (Node node : graph.nodes) {
            compileNodes(nodeData, nodeLookup, allLeft, allTop, new Pair<>(0., 0.), node, 0);
          }
          List<Map<String, Object>> edgeData = new ArrayList<>();
          for (Edge edge : graph.edges) {
            ElkEdge elkEdge = edgeLookup.get(edge.id);
            Map<String, Object> vars = new HashMap<>();
            vars.put("id", edge.id);

            ElkEdgeSection section = elkEdge.getSections().get(0);
            List<Point> points = new ArrayList<>();
            Point basis = new Point();
            basis.x = -allLeft;
            basis.y = -allTop;
            ElkNode at = elkEdge.getContainingNode();
            while (at != null) {
              basis.x += at.getX();
              basis.y += at.getY();
              at = at.getParent();
            }
            Point startPoint = new Point();
            startPoint.x = basis.x + section.getStartX();
            startPoint.y = basis.y + section.getStartY();
            points.add(startPoint);
            for (ElkBendPoint bendPoint : section.getBendPoints()) {
              Point next = new Point();
              next.x = basis.x + bendPoint.getX();
              next.y = basis.y + bendPoint.getY();
              points.add(next);
            }
            Point finalPoint = new Point();
            finalPoint.x = basis.x + section.getEndX();
            finalPoint.y = basis.y + section.getEndY();
            points.add(finalPoint);
            vars.put("points", points);

            vars.put("contents", edge.contents);
            edgeData.add(vars);
          }

          try (OutputStream s_ = Files.newOutputStream(dest);
              Writer s = new OutputStreamWriter(s_)) {
            Templates.loadInMemory(pebble, style.template)
                .evaluate(
                    s,
                    ImmutableMap.<String, Object>builder()
                        .put("edges", edgeData)
                        .put("nodes", nodeData)
                        .put("width", allWidth)
                        .put("height", allHeight)
                        .build());
          }
          return ImmutableList.of();
        });
  }

  public static <T> T loadJson(Path path, Class<T> klass) {
    return uncheck(() -> jackson.readValue(path.toFile(), klass));
  }

  public static String evaluateString(PebbleTemplate template, Map<String, Object> data) {
    Writer s = new StringWriter();
    uncheck(() -> template.evaluate(s, data));
    return s.toString();
  }

  public static void generateNodes(
      GraphStyle style,
      NodeStyleTemplates nodeStyleTemplates,
      ElkNode parent,
      List<DefError> errors,
      Map<String, ElkNode> nodeLookup,
      Node node) {
    ElkNode elkNode = ElkGraphUtil.createNode(parent);
    zeroPaddings(elkNode);
    elkNode.setProperty(LayeredOptions.PADDING, new ElkPadding(style.padding));
    elkNode.setProperty(LayeredOptions.SPACING_NODE_NODE, style.padding);
    elkNode.setProperty(LayeredOptions.SPACING_NODE_NODE_BETWEEN_LAYERS, style.padding);
    elkNode.setProperty(LayeredOptions.SPACING_EDGE_NODE, style.padding);
    elkNode.setProperty(LayeredOptions.SPACING_EDGE_NODE_BETWEEN_LAYERS, style.padding);
    elkNode.setProperty(LayeredOptions.SPACING_EDGE_EDGE, style.padding);
    elkNode.setProperty(LayeredOptions.SPACING_EDGE_EDGE_BETWEEN_LAYERS, style.padding);
    ImmutableMap<String, Object> contents =
        ImmutableMap.<String, Object>of("contents", node.contents);
    elkNode.setProperty(
        LayeredOptions.DIRECTION,
        parseDirection(evaluateString(nodeStyleTemplates.direction, contents)));
    double contentsWidth = Double.parseDouble(evaluateString(nodeStyleTemplates.width, contents));
    elkNode.setWidth(style.padding * 2 + contentsWidth);
    double contentsHeight = Double.parseDouble(evaluateString(nodeStyleTemplates.height, contents));
    elkNode.setHeight(style.padding * 2 + contentsHeight);

    if (node.children.size() > 0) {
      elkNode.setHeight(style.padding * 2);
      elkNode.setWidth(style.padding * 2);
      ElkLabel label = ElkGraphUtil.createLabel(elkNode);
      label.setHeight(contentsHeight);
      label.setWidth(contentsWidth);
      EnumSet<NodeLabelPlacement> placement;
      switch (parseDirection(evaluateString(nodeStyleTemplates.childrenAlignment, contents))) {
        case UP:
          placement = NodeLabelPlacement.insideBottomCenter();
          break;
        case DOWN:
          placement = NodeLabelPlacement.insideTopCenter();
          break;
        case LEFT:
          placement = EnumSet.of(INSIDE, V_CENTER, H_RIGHT);
          break;
        case RIGHT:
          placement = EnumSet.of(INSIDE, V_CENTER, H_LEFT);
          break;
        default:
          throw new Assertion();
      }
      label.setProperty(LayeredOptions.NODE_LABELS_PLACEMENT, placement);
    }

    if (nodeLookup.containsKey(node.id)) {
      errors.add(new DuplicateNode(node.id));
      return;
    }

    nodeLookup.put(node.id, elkNode);

    for (Node child : node.children) {
      generateNodes(style, nodeStyleTemplates, elkNode, errors, nodeLookup, child);
    }
  }

  public static void compileNodes(
      List<Map<String, Object>> s,
      Map<String, ElkNode> nodeLookup,
      double allLeft,
      double allTop,
      Pair<Double, Double> parentPosition,
      Node node,
      int nestDepth)
      throws IOException {
    ElkNode elkNode = nodeLookup.get(node.id);
    Pair<Double, Double> position =
        new Pair<>(parentPosition.first + elkNode.getX(), parentPosition.second + elkNode.getY());
    Map<String, Object> vars = new HashMap<>();
    vars.put("id", node.id);
    vars.put("x", position.first - allLeft);
    vars.put("y", position.second - allTop);
    vars.put("children", !node.children.isEmpty());
    vars.put("width", elkNode.getWidth());
    vars.put("height", elkNode.getHeight());
    vars.put("nestDepth", nestDepth);
    vars.put("contents", node.contents);
    s.add(vars);

    for (Node child : node.children) {
      compileNodes(s, nodeLookup, allLeft, allTop, position, child, nestDepth + 1);
    }
  }

  private static Direction parseDirection(String direction) {
    switch (direction) {
      case "up":
        return Direction.UP;
      case "down":
        return Direction.DOWN;
      case "left":
        return Direction.LEFT;
      case "right":
        return Direction.RIGHT;
      default:
        throw new InvalidDirection(direction);
    }
  }

  public static class NodeStyleTemplates {
    public PebbleTemplate textPadding;
    public PebbleTemplate textHeight;
    PebbleTemplate hintText;
    PebbleTemplate width;
    PebbleTemplate height;
    PebbleTemplate direction;
    PebbleTemplate childrenAlignment;
  }

  public static class InvalidDirection extends RuntimeException {

    public final String direction;

    public InvalidDirection(String direction) {
      this.direction = direction;
    }
  }
}
