package com.zarbosoft.docgraph.core;

import java.util.Map;

public class Edge {
  public String id;
  public String from;
  public String to;
  public String style;
  public Map<String, String> contents;
}
