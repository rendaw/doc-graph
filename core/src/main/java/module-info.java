module com.zarbosoft.docgraph.core {
  exports com.zarbosoft.docgraph.core;
  exports com.zarbosoft.docgraph.core.render;
  exports com.zarbosoft.docgraph.core.render.error;
  requires com.zarbosoft.rendaw.common;
  requires com.zarbosoft.appdirsj;
  requires com.fasterxml.jackson.databind;
  requires io.pebbletemplates;
  requires org.eclipse.elk.alg.layered;
  requires org.eclipse.elk.core;
  requires org.eclipse.elk.graph;
  requires com.google.common;
  requires org.eclipse.emf.common;
  requires yamlbeans;
}