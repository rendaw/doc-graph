**doc♡graph** is a toolset for making graphs/diagrams, using the [ELK](https://github.com/eclipse/elk) layered algorithm

It is three things:

* A simple and ergonomic GUI for editing graphs
* A CLI tool for rendering graphs to SVG without the GUI

![This is what it's like](likethis.png)

## Usage

### GUI

```
docgraph-gui FILENAME
```

`FILENAME` is a file to open to edit, or if it doesn't exist to create (when you first save).

If you don't specify `FILENAME` the GUI will ask you to choose a file when launching.

### CLI

```
docgraph-cli FILENAME OUTPUT.svg
```

This will render `FILENAME` to `OUTPUT.svg`.

#### Hotkeys

* `n` Create node (within the current node)
* `c` Create unrelated node
* `a` Create node after the current node
* `b` Create node before the current node
* `s` Create node as a sibling of the current node
* `left`/`right`/`up`/`down` Depending on the direction of the graph, select first parent/child (only up/down or left/right work at a time).  This is less useful the less linear the graph is.
* `delete` Delete the current node
* `e` Open the properties sidebar
* `esc` Close the properties sidebar
* `right-click` Link selected node to/remove link to the clicked node
* `ctrl + right-click` Add nodes to/remove nodes from the clicked node
* `drag` Scroll graph
* `mouse wheel` Zoom graph
* `click` Select
* `control click` Toggle/add/remove selection
* `ctrl+s` Save
* `ctrl+e` Render

## How to get it

### Packages

Packages for Debian, Arch, and Windows are built automatically:

https://gitlab.com/rendaw/doc-graph/-/pipelines?scope=tags&page=1

Click on the `package` stage (green circle with checkmark) and select your platform, then click "Browse artifacts" in the right tab.

#### Debian

Download the file, then run:

* `dpkg -i PACKAGEFILE`
* `apt-get install -f`

#### Arch

Download the file, then run:

* `pacman -U PACKAGEFILE`

#### Windows

docgraph needs Inkscape for dealing with SVG images.  You can use docgraph on Windows normally as long as you don't use SVG images, but if you want to use SVG images you'll need to have a binary named `inkscape` in a directory listed in your `PATH` environment variable.

### Compile and run it yourself

Make sure you have Inkscape, Python 3, Java and Maven installed.

```
git clone --recurse-submodules https://gitlab.com/rendaw/doc-graph.git
cd doc-graph
./build.py linux linux
```

The first `linux` the platform you're building on, the second `linux` is the target platform (platform on which the binary will run).  You'll need to check `build.py` and make sure paths match up with your system though, they're set mainly for the CI environment.

The minimized standalone java build is in `image/target/image-linux`.

Shell scripts used to run the program are in `packaging/` -- you'll also have to adjust them to where you install the binaries.

## Resources

Icons

* https://github.com/icons8/flat-color-icons
* https://github.com/paomedia/small-n-flat
