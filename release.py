#!/usr/bin/env python3
import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "field", choices=["major", "minor", "patch"], default="patch", nargs="?"
)
args = parser.parse_args()

versions = []
for line in subprocess.check_output(["git", "tag"]).decode("utf-8").splitlines():
    line = line.strip()
    if not line:
        continue
    versions.append(list(map(int, line.split("."))))
versions = sorted(versions, reverse=True)

if versions:
    current_version = versions[0]
else:
    current_version = [0, 0, 1]

field = dict(major=0, minor=1, patch=2)[args.field]
version_parts = []
for i, part in enumerate(current_version):
    if i > field:
        version_parts.append(0)
    elif i == field:
        version_parts.append(part + 1)
    else:
        version_parts.append(part)

version = ".".join(map(str, version_parts))
subprocess.check_call(
    ["git", "tag", "-a", "-s", version, "-m", "RELEASE {}".format(version)]
)
subprocess.check_call(["git", "push", "--follow-tags"])
