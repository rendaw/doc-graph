package com.zarbosoft.docgraph.cli;

import com.zarbosoft.docgraph.core.render.Render;
import com.zarbosoft.docgraph.core.render.error.DefError;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public class Main {
  public static void main(String[] args) {
    uncheck(
        () -> {
          if (args.length != 2) {
            throw new RuntimeException(
                String.format(
                    "Needs two args: graph json file, output svg file; got %s: %s",
                    args.length, Arrays.asList(args)));
          }
          Path source = Paths.get(args[0]).toAbsolutePath();
          Path dest = Paths.get(args[1]);

          List<DefError> errors = Render.render(source, dest);
          if (!errors.isEmpty()) {
            for (DefError error : errors) {
              System.err.format(" - %s\n", error.print());
            }
            System.exit(1);
          }
        });
  }
}
