package com.zarbosoft.embed;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

@Mojo(name = "embed", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class Embed extends AbstractMojo {
  @Parameter(property = "project", readonly = true)
  private MavenProject project;

  @Parameter(property = "sourcePath", defaultValue = "${basedir}/src/main/embed", required = true)
  private String sourcePath;

  public void execute() throws MojoExecutionException, MojoFailureException {
    Path sourceRoot = Paths.get(this.sourcePath);
    Path destRoot = Paths.get(project.getBuild().getDirectory()).resolve("embed");
    project.addCompileSourceRoot(destRoot.toString());
    try {
      Files.walk(sourceRoot)
          .forEach(
              p -> {
                if (!Files.isRegularFile(p)) return;
                try {
                  Path relative = sourceRoot.relativize(p);
                  String name = relative.getFileName().toString().replace(".", "_");
                  Path destParent =
                      relative.getParent() == null
                          ? destRoot
                          : destRoot.resolve(relative.getParent());
                  Path dest = destParent.resolve(name + ".java");
                  Files.createDirectories(destParent);
                  System.out.format("Embedding %s at %s\n", p, dest);
                  try (OutputStream s = Files.newOutputStream(dest)) {
                    s.write(
                        ("package " + relative.getParent().toString().replace("/", ".") + ";\n")
                            .getBytes(StandardCharsets.UTF_8));
                    s.write("import java.util.Base64;\n".getBytes(StandardCharsets.UTF_8));
                    s.write(
                        ("public class " + name + " {        \n").getBytes(StandardCharsets.UTF_8));
                    s.write(
                        ("    public static final byte[] data = Base64.getUrlDecoder().decode(\n        \"")
                            .getBytes(StandardCharsets.UTF_8));
                    s.write(Base64.getUrlEncoder().encode(Files.readAllBytes(p)));
                    s.write(("\"\n    );\n").getBytes(StandardCharsets.UTF_8));

                    s.write(("}\n").getBytes(StandardCharsets.UTF_8));
                  }
                } catch (Exception e) {
                  throw new RuntimeException(e);
                }
              });
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
